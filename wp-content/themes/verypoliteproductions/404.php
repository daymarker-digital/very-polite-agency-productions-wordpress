<?php

  /*
  *
  *	Template Name: 404
  *	Filename: 404.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'error-404';
  $template_classes = $template;
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  // ---------------------------------------- AOS Data
  $aos_id = $template_id;
  $aos_delay = 350;
  $aos_increment = 150;

  // ---------------------------------------- ACF Data
  $acf_data = get_field( 'error_404', 'options' ) ?: [];
  $cols = 'col-12';
  $container = 'container';
  $heading = $acf_data['heading'] ?? '';
  $message = $acf_data['message'] ?? '';

?>

<section class="<?= esc_attr( $template_classes ); ?>" id="<?= esc_attr( $template_id ); ?>">
  <div class="<?php echo $template; ?>__main">
    <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
      <div class="<?php echo $template; ?>__main-content">

        <?php if ( $heading ) : ?>
          <?php
            $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'offset' => 0, 'transition' => 'fade-left' ]);
            $aos_delay += $aos_increment;
          ?>
          <h1 class="<?php echo $template; ?>__heading heading--primary" <?= $aos_attrs; ?>><?= $heading; ?></h1>
        <?php endif; ?>

        <?php if ( $message ) : ?>
          <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
          <div class="<?php echo $template; ?>__message body-copy--primary body-copy--1" <?= $aos_attrs; ?>><?= $message; ?></div>
        <?php endif; ?>

      </div>
    <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
  </div>
</section>

<?php get_footer(); ?>
