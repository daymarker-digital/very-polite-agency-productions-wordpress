<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

<head>

  <?php include( locate_template( './snippets/google-tag-manager.php' ) ); ?>

  <title><?php wp_title(''); ?></title>

   <?php

    $THEME = $THEME ?? new CustomTheme();
    $assets_dir = $THEME->get_theme_directory('assets');
    $theme_dir = $THEME->get_theme_directory();
    $theme_classes = $THEME->get_theme_classes();
    $object_id = $THEME->get_theme_info('object_ID');
    $post_id = $THEME->get_theme_info('post_ID');

    echo $THEME->render_preconnect_resources([ 'https://cdn.jsdelivr.net', 'https://www.google-analytics.com' ]);

  ?>

  <link rel="preload" href="<?= $assets_dir; ?>/main.min.js?ver=<?= filemtime( get_template_directory() . '/assets/main.min.js' ); ?>" as="script">

  <link rel="apple-touch-icon" href="<?= $theme_dir; ?>/apple-touch-icon.png?v=<?= filemtime( get_template_directory() . '/apple-touch-icon.png' ); ?>">
  <link rel="shortcut icon" href="<?= $theme_dir; ?>/favicon.ico?v=<?= filemtime( get_template_directory() . '/favicon.ico' ); ?>">

  <meta charset="<?= get_bloginfo('charset'); ?>">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Very Polite">
  <meta http-equiv="Expires" content="7" />

  <?php

    // ---------------------------------------- Preload Fonts
    echo $THEME->render_preload_fonts([
      'AkkuratMonoLLWeb-Regular',
      'AkkuratMonoLLTT-Bold',
      'SorrySans-BoldExtended'
    ]);

    // ---------------------------------------- SEO
    echo $THEME->render_seo();

    // ---------------------------------------- External JavaScript
    include( locate_template( "./snippets/additional-javascript.php" ) );

    // ---------------------------------------- WP Head Hook
    wp_head();

  ?>

</head>

<body class='<?= $theme_classes; ?> sticky-footer' data-object-id='<?= $object_id; ?>' data-post-id='<?= $post_id; ?>'>

  <?php

    // ---------------------------------------- Google Tag Manager No Script
    include( locate_template( './snippets/google-tag-manager-no-script.php' ) );

    // ---------------------------------------- Site Vitals
    echo ( is_user_logged_in() || is_admin() ) ? $THEME->get_theme_info( 'vitals' ) : '';

    // ---------------------------------------- Drawer
    include( locate_template( './snippets/drawer.php' ) );

    // ---------------------------------------- Header
    include( locate_template( './snippets/header.php' ) );

  ?>

  <main class="<?= $theme_classes; ?>" role="main">
