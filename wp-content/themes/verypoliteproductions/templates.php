<?php

class PDTemplates extends PoliteDepartment {

  private $version = 1.0;

  public $navigation = [
    [
      'classes' => '',
      'title' => '<span>+</span>Services',
      'link' => '#services',
      'target' => '',
    ],
    [
      'classes' => '',
      'title' => '<span>+</span>Studio',
      'link' => '#studio',
      'target' => '',
    ],
    [
      'classes' => '',
      'title' => '<span>+</span>Pricing',
      'link' => '#pricing',
      'target' => '',
    ],
    [
      'classes' => '',
      'title' => '<span>+</span>Contact',
      'link' => '#footer',
      'target' => '',
    ],
    [
      'classes' => 'outline',
      'title' => 'The Agency',
      'link' => 'https://weareverypolite.com',
      'target' => '_blank',
    ]
  ];

  public $address = [
    'street' => '525 Seymout Street',
    'street_2' => '601',
    'city' => 'Vancouver',
    'region' => 'BC',
    'postal' => 'V6B 3H6',
    'country' => 'Canada',
    'business' => 'Very Polite Agency'
  ];

  /*
  //////////////////////////////////////////////////////////
  ////  Render
  //////////////////////////////////////////////////////////
  */

  public function render ( $template = false, $params = [] ) {
    switch ( $template ) {
      case 'hero':
        return $this->render_hero( $params );
        break;
      case 'google-maps-link':
        return $this->render_google_maps_link( $params );
        break;
      case 'image-feature':
        return $this->render_image_feature( $params );
        break;
      case 'media-with-text':
        return $this->render_media_with_text( $params );
        break;
      case 'pricing':
        return $this->render_pricing( $params );
        break;
      case 'selected-clients':
        return $this->render_selected_clients( $params );
        break;
      default:
        return '<h2 style="background: #000; color: #fff; text-align: center;">No such template configured!</h2>';
      }
  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Carousel
  //////////////////////////////////////////////////////////
  */

  public function render_carousel ( $params = [] ) {

    $html = '';
    $id = $items = false;
    $control_next_arrow = $this->get_theme_directory('assets') . '/img/VPP--carousel--next-arrow--white.svg';

    if ( $params ) {

      // extract params
      extract( $params );

      if ( $id && $items ) {

        $id = clean_string( $id );

        $html .= '<div class="carousel ' . $id . '">';
          $html .= '<div class="carousel__controls">';
            $html .= '<div class="carousel__prev">';
              $html .= '<img src="' . $control_next_arrow . '" alt="Prev" />';
            $html .= '</div>';
            $html .= '<div class="carousel__next">';
              $html .= '<img src="' . $control_next_arrow . '" alt="Next" />';
            $html .= '</div>';
          $html .= '</div>';
          $html .= '<div class="carousel__main" id="carousel__' . $id . '">';
            foreach( $items as $i => $item ) {
              $html .= '<div class="carousel__item">';
                $html .= $this->lazyload_image( $item['image'] );
              $html .= '</div>';
            }
          $html .= '</div>';
        $html .= '</div>';
      }

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Contact Us
  //////////////////////////////////////////////////////////
  */

  public function render_contact_us ( $params = [] ) {

    $html = '';
    $address = $this->address;
    $street = $street_2 = false;
    $block_name = 'contact-us';
    $icon_set = $this->get_theme_directory('assets') . '/img/VPP--branding--icon-set.svg';
    $email = 'productions@weareverypolite.com';
    $phone = '(604) 428 6177';

    if ( isset($address['street']) && !empty($address['street']) ) {
      $street = $address['street'];
      if ( isset($address['street_2']) && !empty($address['street_2']) ) {
        $street = $address['street_2'] . ' – ' . $street;
      }
    }

    if ( isset($address['city']) && !empty($address['city']) ) {
      $street .= '<br>' . $address['city'];
      if ( isset($address['region']) && !empty($address['region']) ) {
        $street .= ' ' . $address['region'];
      }
      if ( isset($address['postal']) && !empty($address['postal']) ) {
        $street .= ' ' . $address['postal'];
      }
    }

    $html .= '<div class="'. $block_name . '">';
      $html .= '<div class="' . $block_name .'__main">';

        $html .= '<h3 class="' . $block_name .'__heading">Very Polite Productions Ltd.</h3>';

        $html .= '<div class="' . $block_name .'__content rte">';
          if ( $street ) {
            $html .= '<p><a href="' . $this->render_google_maps_link( $address ) . '" target="_blank">' . $street . '</a></p>';
          }
          if ( $email ) {
            $html .= '<p><a href="mailto:' . $email . '">' . $email . '</a></p>';
          }
          if ( $phone ) {
            $html .= '<p><a href="tel:' . $phone . '">' . $phone . '</a></p>';
          }
        $html .= '</div>';

        $html .= '<div class="' . $block_name .'__icon-set">';
          $html .= '<img src="' . $icon_set . '" />';
        $html .= '</div>';

        $html .= '<div class="' . $block_name .'__legal rte">';
          $html .= '<p><a href="https://weareverypolite.com" target="_blank">C/O Very Polite Agency, Inc.</a></p>';
        $html .= '</div>';

      $html .= '</div>';
    $html .= '</div>';

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Container
  //////////////////////////////////////////////////////////
  */

  public function render_bs_container ( $state = 'open', $col = 'col-12' ) {

    $html = '<div class="container"><div class="row"><div class="' . $col . '">';

    if ( 'open' !== $state ) {
      $html = '</div></div></div>';
    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Google Maps Link
  //////////////////////////////////////////////////////////
  */

  public function render_google_maps_link ( $params = [] ) {

    $html = '';
    $base = 'https://maps.google.com/?q=';

    if ( $params ) {

      // default data
      $business = $city = $country = $region = $postal = $street = $street_2 = false;

      // extract $params
      extract( $params );

      if ( $street ) {
        $html .= $street;
        if ( $street_2 ) {
          $html = $street_2 . '–' . $street;
        }
      }
      if ( $city ) {
        $html .= ' ' . $city;
      }
      if ( $region ) {
        $html .= ' ' . $region;
      }
      if ( $postal ) {
        $html .= ' ' . $postal;
      }
      if ( $country ) {
        $html .= ' ' . $country;
      }
      if ( $business ) {
        $html .= $business . ' ' . $html;
      }

      if ( $html ) {
        $html = $base . trim($html);
      }

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Hero
  //////////////////////////////////////////////////////////
  */

  public function render_hero ( $params = [] ) {

    $html = '';
    $block_name = 'hero';
    $include_container = false;

    if ( $params ) {

      // default data
      $heading = $image = $message = $section_id = false;

      // extract $params
      extract( $params );

      if ( $heading || $message || $image ) {

        $html .= '<section class="section section--' . $block_name . ' ' . $block_name . '" id="' . ( $section_id ? $section_id : clean_string( $heading ) ) . '">';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          }

            $html .= '<div class="' . $block_name . '__content">';

              $html .= '<div class="' . $block_name . '__media">';
                if ( $image ) {
                  $html .= '<div class="' . $block_name . '__image">';
                    $html .= $this->lazyload_image( $image );
                  $html .= '</div>';
                  $html .= '<div class="' . $block_name . '__branding ' . $block_name . '__branding--approved">';
                    $html .= '<img src="' . $this->get_theme_directory('assets') . '/img/VPP--branding--approved.svg" />';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="' . $block_name . '__text">';
                if ( $heading ) {
                  $html .= '<h1 class="' . $block_name . '__heading heading heading--page-title">' . $heading . '</h1>';
                }
                if ( $message ) {
                  $html .= ' <div class="' . $block_name . '__message message rte">' . $message . '</div>';
                }
             $html .= '</div>';

           $html .= '</div>';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'close' );
          }

        $html .= '</section>';

      }

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Image Feature
  //////////////////////////////////////////////////////////
  */

  public function render_image_feature ( $params = [] ) {

    $html = '';
    $block_name = 'image-feature';
    $include_container = false;

    if ( $params ) {

      // default data
      $heading = $message = $subheading = $section_id = false;

      // extract $params
      extract( $params );

      $html .= '<section class="section section--' . $block_name . ' ' . $block_name . '" id="' . ( $section_id ? $section_id : clean_string( $heading ) ) . '">';
        $html .= '<div class="' . $block_name . '__main main">';

          if ( $heading ) {
            $html .= '<h2 class="' . $block_name . '__heading heading heading--primary">' . $heading . '</h2>';
          }

          if ( $image ) {
            $html .= '<div class="' . $block_name . '__image">';
              $html .= $this->lazyload_image( $image, [ 'alt_text' => $heading ] );
            $html .= '</div>';
          }

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          }

          if ( $subheading || $message ) {
            $html .= '<div class="' . $block_name . '__content">';
              if ( $subheading ) {
                $html .= '<h3 class="' . $block_name . '__heading heading heading--secondary">' . $subheading . '</h3>';
              }
              if ( $message ) {
                $html .= '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
              }
            $html .= '</div>';
          }

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'close' );
          }

        $html .= '</div>';
      $html .= '</section>';

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Media with Text
  //////////////////////////////////////////////////////////
  */

  public function render_media_with_text ( $params = [] ) {

    // defaults
    $html = '';
    $block_name = 'media-with-text';
    $include_container = false;

    if ( $params ) {

      // defaults
      $heading = $items = $section_id = false;

      // deconstruct $params
      extract( $params );

      $html .= '<section class="section section--' . $block_name . ' ' . $block_name . '" id="' . ( $section_id ? $section_id : clean_string( $heading ) ) . '">';
        $html .= '<div class="' . $block_name . '__main main">';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          }

            if ( $heading ) {
              $html .= '<h2 class="' . $block_name . '__heading heading heading--primary">' . $heading . '</h2>';
            }

            if ( $items ) {
              foreach( $items as $item ) {

                // default data
                $heading = $image = $layout = $media_type = $message = false;

                // extract data
                extract( $item );

                $html .= '<div class="' . $block_name . '__item" data-layout="' . $layout . '">';

                  switch ( $media_type ) {
                    case 'image':
                      if ( $image ) {
                        $html .= '<div class="' . $block_name . '__media image">';
                          $html .= $this->lazyload_image( $image );
                        $html .= '</div>';
                      }
                      break;
                  }

                  if ( $heading || $message ) {
                    $html .= '<div class="' . $block_name . '__content">';
                      if ( $heading ) {
                        $html .= '<h3 class="' . $block_name . '__heading heading heading--secondary">' . $heading . '</h3>';
                      }
                      if ( $message ) {
                        $html .= '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
                      }
                    $html .= '</div>';
                  }

                $html .= '</div>';

              }
            }

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'close' );
          }

        $html .= '</div>';
      $html .= '</section>';

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Navigation
  //////////////////////////////////////////////////////////
  */

  public function render_navigation ( $params = [] ) {

    // default data
    $html = '';
    $block_name = 'navigation';

    // $params defaults
    $classes = false;
    $navigation = $this->navigation;

    if ( $params ) {
      extract( $params );
    }

    if ( $navigation ) {
      $html .= '<nav class="' . $block_name . ( $classes ? ' ' . $classes : '' ) . '">';
        $html .= '<ul class="' . $block_name . '__list">';
          foreach( $navigation as $i => $item ) {

            // default data
            $classes = $link = $target = $title = false;

            // extract $item
            extract( $item );

            if ( $link && $title ) {
              $html .= '<li class="' . $block_name . '__item' . ( $classes ? ' ' . $classes : '' ) . '">';
                $html .= '<a class="' . $block_name . '__link" href="' . $link . '" ' . ( $target ? 'target="' . $target . '"' : '' ) . '>' . $title . '</a>';
              $html .= '</li>';
            }

          }
        $html .= '</ul>';
      $html .= '</nav>';
    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Render | Pricing
  //////////////////////////////////////////////////////////
  */

  public function render_pricing ( $params = [] ) {

    // defaults
    $html = '';
    $block_name = 'pricing';
    $include_container = false;

    if ( $params ) {

      // defaults
      $carousel = $disclaimer = $heading = $image = $section_id = false;

      // deconstruct $params
      extract( $params );

      $html .= '<section class="section section--' . $block_name . ' ' . $block_name . '" id="' . ( $section_id ? $section_id : clean_string( $heading ) ) . '">';
        $html .= '<div class="' . $block_name . '__main main">';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          }

            if ( $carousel ) {
              $html .= '<div class="' . $block_name . '__content d-md-none">';
                $html .= $this->render_carousel( $carousel );
              $html .= '</div>';
            }

            if ( $image ) {
              $html .= '<div class="' . $block_name . '__image d-none d-md-block">';
                $html .= $this->lazyload_image( $image );
              $html .= '</div>';
            }

            if ( $disclaimer ) {
              $html .= '<div class="' . $block_name . '__disclaimer rte">' . $disclaimer . '</div>';
            }

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'close' );
          }

        $html .= '</div>';
      $html .= '</section>';

    }

    return $html;

  }

  // ---------------------------------------- Selected Clients
  public function render_selected_clients ( $params = [] ) {

    // defaults
    $html = '';
    $block_name = 'selected-clients';
    $include_container = false;
    $clients = [
      "34 Heritage",
      "Aedelhard",
      "ALT",
      "Another Room",
      "Arlo Kombucha",
      "Bash + Fete",
      "B.O.A.",
      "Bon Look",
      "Bootlegger",
      "Bruush",
      "Canada Goose",
      "Chip Wilson",
      "Eavor",
      "Face Of Today",
      "Fairmont Pac Rim",
      "The Flying Pig",
      "Gillian Segal Design",
      "Holborn",
      "Hootsuite",
      "House Concepts",
      "Kit And Ace",
      "Kits Eyewear",
      "Mackage",
      "Mary Rich",
      "Mavi",
      "Mr. Gray",
      "Oak + Fort",
      "Ocin",
      "Purple Denim",
      "Ride Cycle Club",
      "Shaketown Brewing",
      "Shape Properties",
      "Sorry Coffee Co.",
      "Spence Diamonds",
      "Tee Beverages",
      "Token",
      "Torpedo Publishing",
      "West Coast Kids",
      "Westbank",
    ];

    if ( $clients && $params ) {

      // default data
      $heading = $section_id = false;

      // extract $params
      extract( $params );

      $html .= '<section class="section section--' . $block_name . ' ' . $block_name . '" id="' . ( $section_id ? $section_id : clean_string( $heading ) ) . '">';
        $html .= '<div class="' . $block_name . '__main main">';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          }

            $html .= '<div class="' . $block_name . '__content">';

              if ( $heading ) {
                $html .= '<h2 class="' . $block_name . '__heading heading heading--secondary">' . $heading . '</h2>';
              }

              $html .= '<div class="' . $block_name . '__grid">';

                //////////////////////////////////////////////////////////
                ////  Columns | Mobile
                //////////////////////////////////////////////////////////

                $limit = 20;
                $html .= '<div class="' . $block_name . '__column d-md-none" data-column="left">';
                  foreach( $clients as $i => $client ) {
                    if ( $i < $limit ) {
                      $html .= '<span class="' . $block_name . '__item" data-count="' . $i . '">' . $client . '</span>';
                    } else {
                      break;
                    }
                  }
                $html .= '</div>';
                $html .= '<div class="' . $block_name . '__column d-md-none" data-column="right">';
                  foreach( $clients as $i => $client ) {
                    if ( $i >= $limit ) {
                      $html .= '<span class="' . $block_name . '__item" data-count="' . $i . '">' . $client . '</span>';
                    }
                  }
                $html .= '</div>';

                //////////////////////////////////////////////////////////
                ////  Columns | Desktop
                //////////////////////////////////////////////////////////

                $limit = 13;
                $html .= '<div class="' . $block_name . '__column d-none d-md-block" data-column="left">';
                  foreach( $clients as $i => $client ) {
                    if ( $i < $limit ) {
                      $html .= '<span class="' . $block_name . '__item" data-count="' . $i . '">' . $client . '</span>';
                    } else {
                      break;
                    }
                  }
                $html .= '</div>';
                $html .= '<div class="' . $block_name . '__column d-none d-md-block" data-column="middle">';
                  foreach( $clients as $i => $client ) {
                    if ( ($i >= $limit) && ($i < ( $limit * 2 )) ) {
                      $html .= '<span class="' . $block_name . '__item" data-count="' . $i . '">' . $client . '</span>';
                    }
                  }
                $html .= '</div>';
                $html .= '<div class="' . $block_name . '__column d-none d-md-block" data-column="right">';
                  foreach( $clients as $i => $client ) {
                    if ( $i >= ( $limit * 2 ) ) {
                      $html .= '<span class="' . $block_name . '__item" data-count="' . $i . '">' . $client . '</span>';
                    }
                  }
                $html .= '</div>';



              $html .= '</div>';

            $html .= '</div>';

          if ( $include_container ) {
            $html .= $this->render_bs_container( 'close' );
          }

        $html .= '</div>';
      $html .= '</section>';

    }

    return $html;

  }

  // --------------------------- SVG Icon
  public function render_svg( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => 'burger'
      ],
      $params
    ));

    switch( $type ) {

      case 'icon.email': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <path  d="M36,8.1C36,8,36,8,36,7.9c-0.1-2.5-2.2-4.6-4.8-4.6H4.8c-2.6,0-4.7,2-4.8,4.6C0,7.9,0,8,0,8.1c0,0,0,0,0,0v19.8
	        c0,2.7,2.2,4.8,4.8,4.8h26.4c2.7,0,4.8-2.2,4.8-4.8L36,8.1C36,8.1,36,8.1,36,8.1z M4.8,6.3h26.4c0.7,0,1.4,0.4,1.6,1.1L18,17.8
	        L3.2,7.4C3.5,6.8,4.1,6.3,4.8,6.3z M33,27.9c0,1-0.8,1.8-1.8,1.8H4.8c-1,0-1.8-0.8-1.8-1.8V11l14.1,9.9c0.3,0.2,0.6,0.3,0.9,0.3
	        s0.6-0.1,0.9-0.3L33,11V27.9z"/>
        </svg>';
        break;
      }

      case 'icon.instagram': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <g>
	          <path d="M35.9,10.6c0-1.5-0.3-3-0.8-4.4C34.6,5,33.9,3.9,33,3c-0.9-0.9-2-1.6-3.2-2.1c-1.4-0.5-2.9-0.8-4.4-0.8
		          C23.5,0,22.9,0,18,0c-4.9,0-5.5,0-7.4,0.1c-1.5,0-3,0.3-4.4,0.8C5,1.4,3.9,2.1,3,3C2.1,3.9,1.4,5,0.9,6.2c-0.5,1.4-0.8,2.9-0.8,4.4
		          C0,12.5,0,13.1,0,18c0,4.9,0,5.5,0.1,7.4c0,1.5,0.3,3,0.8,4.4C1.4,31,2.1,32.1,3,33c0.9,0.9,2,1.6,3.2,2.1c1.4,0.5,2.9,0.8,4.4,0.8
		          C12.5,36,13.1,36,18,36c4.9,0,5.5,0,7.4-0.1c1.5,0,3-0.3,4.4-0.8c1.2-0.5,2.3-1.2,3.2-2.1c0.9-0.9,1.6-2,2.1-3.2
		          c0.5-1.4,0.8-2.9,0.8-4.4C36,23.5,36,22.9,36,18C36,13.1,36,12.5,35.9,10.6z M32.7,25.3c0,1.1-0.2,2.3-0.6,3.3
		          c-0.3,0.8-0.8,1.5-1.3,2.1c-0.6,0.6-1.3,1-2.1,1.3c-1.1,0.4-2.2,0.6-3.3,0.6c-1.9,0.1-2.5,0.1-7.3,0.1c-4.8,0-5.4,0-7.3-0.1
		          c-1.1,0-2.3-0.2-3.3-0.6c-0.8-0.3-1.5-0.8-2.1-1.3c-0.6-0.6-1.1-1.3-1.3-2.1c-0.4-1.1-0.6-2.2-0.6-3.3c-0.1-1.9-0.1-2.5-0.1-7.3
		          c0-4.8,0-5.4,0.1-7.3c0-1.1,0.2-2.3,0.6-3.3c0.3-0.8,0.8-1.5,1.3-2.1C5.9,4.7,6.6,4.3,7.4,4c1.1-0.4,2.2-0.6,3.3-0.6
		          c1.9-0.1,2.5-0.1,7.3-0.1l0,0c4.8,0,5.4,0,7.3,0.1c1.1,0,2.3,0.2,3.3,0.6c0.8,0.3,1.5,0.8,2.1,1.3c0.6,0.6,1.1,1.3,1.3,2.1
		          c0.4,1.1,0.6,2.2,0.6,3.3c0.1,1.9,0.1,2.5,0.1,7.3C32.8,22.8,32.7,23.4,32.7,25.3z"/>
	          <path d="M24.5,11.5c-0.9-0.9-1.9-1.5-3-2C20.4,9,19.2,8.8,18,8.8c-1.8,0-3.6,0.5-5.1,1.6c-1.5,1-2.7,2.5-3.4,4.1
		          c-0.7,1.7-0.9,3.5-0.5,5.3c0.4,1.8,1.2,3.4,2.5,4.7c1.3,1.3,2.9,2.2,4.7,2.5c1.8,0.4,3.7,0.2,5.3-0.5c1.7-0.7,3.1-1.9,4.1-3.4
		          c1-1.5,1.6-3.3,1.6-5.1c0-1.2-0.2-2.4-0.7-3.5C26.1,13.3,25.4,12.3,24.5,11.5z M22.2,22.2C21.1,23.4,19.6,24,18,24
		          c-1.2,0-2.3-0.4-3.3-1s-1.8-1.6-2.2-2.7c-0.5-1.1-0.6-2.3-0.3-3.5c0.2-1.2,0.8-2.2,1.6-3.1c0.8-0.8,1.9-1.4,3.1-1.6
		          s2.4-0.1,3.5,0.3c1.1,0.5,2,1.2,2.7,2.2s1,2.1,1,3.3C24,19.6,23.4,21.1,22.2,22.2z"/>
	          <path d="M27.6,6.2c-1.2,0-2.2,1-2.2,2.2c0,1.2,1,2.2,2.2,2.2c1.2,0,2.2-1,2.2-2.2C29.8,7.2,28.8,6.2,27.6,6.2z"/>
          </g>
        </svg>';
        break;
      }

      case 'icon.spotify': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <path d="M18,0C8.1,0,0,8.1,0,18s8.1,18,18,18s18-8.1,18-18S27.9,0,18,0z M26.3,26.1c-0.5,0.4-0.9,0.7-1.6,0.4
	        c-4.3-2.7-9.4-3.1-15.8-1.8c-0.7,0.2-1.1-0.2-1.3-0.9c-0.2-0.7,0.2-1.1,0.9-1.4c6.8-1.6,12.8-0.9,17.5,2
	        C26.5,24.8,26.5,25.4,26.3,26.1z M28.4,21.1c-0.5,0.7-1.4,0.9-1.8,0.5c-4.9-2.9-12.1-3.8-18-2c-0.7,0-1.4-0.2-1.6-1.1
	        c-0.2-0.7,0.2-1.4,0.9-1.6c6.5-2,14.6-0.9,20.2,2.5C28.6,19.6,28.8,20.5,28.4,21.1z M28.6,16c-5.9-3.4-15.3-3.8-20.9-2
	        c-0.9,0.2-1.8-0.2-2-1.1c-0.2-0.9,0.2-1.8,1.1-2c6.3-1.8,16.9-1.6,23.6,2.5c0.9,0.5,1.1,1.6,0.7,2.2C30.6,16.2,29.5,16.4,28.6,16z"
	        />
        </svg>';
        break;
      }

    }

    return $html;

  }

}

?>
