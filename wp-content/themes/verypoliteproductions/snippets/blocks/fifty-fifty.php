<?php

  /**
  *
  *   Fifty-Fifty
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'fifty-fifty';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $heading = get_field('heading') ?: '';
  $image = get_field('image') ?: [];
  $image_aspect_ratio = get_field('image_aspect_ratio') ?: '';
  $image_extra = get_field('image_extra') ?: [];
  $layout_gutter = get_field('layout_gutter') ?: 0;
  $layout_order = get_field('layout_order') ?: 'image-left';
  $message = get_field('message') ?: '';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
    <?php if ( $layout_gutter ) : ?>
      @media screen and (min-width: 768px) {
        #<?= $block_id; ?> .grid {
          gap: <?= $layout_gutter; ?>px;
        }
      }
    <?php endif; ?>
    <?php if ( $image_aspect_ratio ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__image {
        aspect-ratio: <?= $image_aspect_ratio; ?>
      }
    <?php endif; ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?> <?= $layout_order; ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content grid grid--two-column">

          <?php if ( !empty($image) ) : ?>
            <div class="<?= $block_name; ?>__item grid__item grid__item--image">
              <?php
                $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]);
                $aos_delay += $aos_increment;
              ?>
              <div class="<?= $block_name; ?>__image image <?= $image_aspect_ratio ? ' with-aspect-ratio' : ''; ?>" <?= $aos_attrs; ?>>
                <?= $THEME->render_lazyload_image([ 'image' => $image ]); ?>
              </div>
              <?php if ( !empty($image_extra) ) : ?>
                <?php
                  $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]);
                  $aos_delay += $aos_increment;
                ?>
                <div class="<?= $block_name; ?>__image-extra" <?= $aos_attrs; ?>>
                  <?= $THEME->render_lazyload_image([ 'image' => $image_extra ]); ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>

          <?php if ( $heading || $message ) : ?>
            <div class="<?= $block_name; ?>__item grid__item grid__item--content">
              <?php if ( $heading ) : ?>
                <?php
                  $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
                  $aos_delay += $aos_increment;
                ?>
                <strong class="<?= $block_name; ?>__heading heading--primary heading--md text--uppercase" <?= $aos_attrs; ?>><?= $heading; ?></strong>
              <?php endif; ?>
              <?php if ( $message ) : ?>
                <?php
                  $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
                  $aos_delay += $aos_increment;
                ?>
                <div class="<?= $block_name; ?>__message body-copy--primary body-copy--3" <?= $aos_attrs; ?>><?= $message; ?></div>
              <?php endif; ?>
            </div>
          <?php endif; ?>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>
<?php endif; ?>
