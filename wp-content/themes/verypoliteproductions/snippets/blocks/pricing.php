<?php

  /**
  *
  *   Pricing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'pricing';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $carousel_animation_duration = get_field('carousel_animation_duration');
  $carousel_autoplay = get_field('carousel_autoplay');
  $carousel_id = "{$block_id}--carousel";
  $carousel_items = get_field('carousel_items') ?: [];
  $chart = get_field('chart') ?: [];
  $disclaimer = get_field('disclaimer') ?: '';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
    <?php if ( $layout_gutter ) : ?>
      @media screen and (min-width: 768px) {
        #<?= $block_id; ?> .grid {
          gap: <?= $layout_gutter; ?>px;
        }
      }
    <?php endif; ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content">

          <?php if ( !empty($chart) ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); ?>
            <div class="<?= $block_name; ?>__chart d-none d-md-block" <?= $aos_attrs; ?>>
              <?= $THEME->render_lazyload_image([ 'image' => $chart ]); ?>
            </div>
          <?php endif; ?>

          <?php if ( !empty($carousel_items) && count($carousel_items) > 1 ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); ?>
            <div
              class="glide js--glide d-md-none"
              id="<?= $carousel_id; ?>"
              data-glide-animation-duration="<?= $carousel_animation_duration; ?>"
              data-glide-autoplay="<?= $carousel_autoplay; ?>"
              data-glide-gap="0"
              data-glide-style="<?= $block_name; ?>"
              <?= $aos_attrs; ?>
            >

              <button class="glide__button button button--glide-nav prev disabled" data-target="#<?= $carousel_id; ?>" type="button" aria-label="Previous">
                <?= $THEME->render_svg_icon([ 'type' => 'arrow.slim' ]); ?>
              </button>
              <button class="glide__button button button--glide-nav next disabled" data-target="#<?= $carousel_id; ?>" type="button" aria-label="Next">
                <?= $THEME->render_svg_icon([ 'type' => 'arrow.slim' ]); ?>
              </button>

              <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                  <?php foreach ( $carousel_items as $item ) : $image = $item['image'] ?? []; ?>
                    <?php if ( !empty($image) ) : ?>
                      <li class="glide__slide">
                        <div class="<?= $block_name; ?>__item">
                          <?= $THEME->render_lazyload_image([ 'image' => $image ]); ?>
                        </div>
                      </li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              </div>

            </div>
          <?php endif; ?>

          <?php if ( $disclaimer ) : ?>
            <?php
              $aos_delay += $aos_increment;
              $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
            ?>
            <div class="<?= $block_name; ?>__disclaimer body-copy--primary body-copy--3 text--align-center" <?= $aos_attrs; ?>><?= $disclaimer; ?></div>
          <?php endif; ?>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>
<?php endif; ?>
