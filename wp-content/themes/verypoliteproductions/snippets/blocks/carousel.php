<?php

  /**
  *
  *   Carousel
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'carousel';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $carousel_animation_duration = get_field('carousel_animation_duration');
  $carousel_autoplay = get_field('carousel_autoplay');
  $carousel_direction = get_field('carousel_direction');
  $carousel_gap = get_field('carousel_gap');
  $carousel_id = "{$block_id}--carousel";
  $carousel_items = get_field('images') ?: [];
  $image_aspect_ratio = get_field('image_aspect_ratio') ?: '';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
    <?php if ( $image_aspect_ratio ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__item {
        aspect-ratio: <?= $image_aspect_ratio; ?>
      }
    <?php endif; ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content">

          <?php if ( !empty($carousel_items) && count($carousel_items) > 1 ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); ?>
            <div
              class="glide js--glide"
              id="<?= $carousel_id; ?>"
              data-glide-animation-duration="<?= $carousel_animation_duration; ?>"
              data-glide-autoplay="<?= $carousel_autoplay; ?>"
              data-glide-gap="<?= $carousel_gap; ?>"
              data-glide-direction="<?= $carousel_direction; ?>"
              data-glide-style="<?= $block_name; ?>"
              <?= $aos_attrs; ?>
            >

              <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                  <?php foreach ( $carousel_items as $item ) : $image = $item['image'] ?? []; ?>
                    <?php if ( !empty($image) ) : ?>
                      <li class="glide__slide">
                        <div class="<?= $block_name; ?>__item <?= $image_aspect_ratio ? ' with-aspect-ratio' : ''; ?>">
                          <?= $THEME->render_lazyload_image([ 'image' => $image ]); ?>
                        </div>
                      </li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              </div>

            </div>
          <?php endif; ?>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>
<?php endif; ?>
