<?php

  /**
  *
  *   Client Listing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'client-listing';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12 col-lg-10 offset-lg-1';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $clients = [
    "34 Heritage",
    "Aedelhard",
    "ALT",
    "Another Room",
    "Arlo Kombucha",
    "Bash + Fete",
    "B.O.A.",
    "Bon Look",
    "Bootlegger",
    "Bruush",
    "Canada Goose",
    "Chip Wilson",
    "Eavor",
    "Face Of Today",
    "Fairmont Pac Rim",
    "The Flying Pig",
    "Gillian Segal Design",
    "Holborn",
    "Hootsuite",
    "House Concepts",
    "Kit And Ace",
    "Kits Eyewear",
    "Mackage",
    "Mary Rich",
    "Mavi",
    "Mr. Gray",
    "Oak + Fort",
    "Ocin",
    "Purple Denim",
    "Ride Cycle Club",
    "Shaketown Brewing",
    "Shape Properties",
    "Sorry Coffee Co.",
    "Spence Diamonds",
    "Tee Beverages",
    "Token",
    "Torpedo Publishing",
    "West Coast Kids",
    "Westbank",
  ];
  $clients_size = count( $clients );
  $heading = get_field('heading') ?: '';
  $heading_size = 'md';
  $heading_style = 'primary';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content text--align-center">

          <?php if ( $heading ) : ?>
            <?php
              $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]);
              $aos_delay += $aos_increment;
            ?>
            <h2 class="<?= $block_name; ?>__heading heading--<?= $heading_style; ?> heading--<?= $heading_size; ?>" <?= $aos_attrs; ?>><?= $heading; ?></h2>
          <?php endif; ?>

          <?php
            $columns = 2;
            $remainder = $clients_size%$columns;
            $limit = $remainder ? floor($clients_size/$columns) + $remainder : floor($clients_size/$columns);
            $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
          ?>
          <div class="<?= $block_name; ?>__grid body-copy--3 d-grid d-md-none" <?= $aos_attrs; ?>>
            <div class="<?= $block_name; ?>__column">
              <?php foreach( $clients as $i => $client ) : ?>
                <?php if ( $i < $limit ) : ?>
                  <div class='<?= $block_name; ?>__item' data-index0='<?= $i; ?>'><?= $client; ?></div>
                <?php else : break; endif; ?>
              <?php endforeach; ?>
            </div>
            <div class="<?= $block_name; ?>__column">
              <?php foreach( $clients as $i => $client ) : ?>
                <?php if ( $i >= $limit ) : ?>
                  <div class='<?= $block_name; ?>__item' data-index0='<?= $i; ?>'><?= $client; ?></div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
          </div>

          <?php
            $columns = 3;
            $remainder = $clients_size%$columns;
            $limit = $remainder ? floor($clients_size/$columns) + $remainder : floor($clients_size/$columns);
            $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
          ?>
          <div class="<?= $block_name; ?>__grid body-copy--3 d-none d-md-grid" <?= $aos_attrs; ?>>
            <div class="<?= $block_name; ?>__column">
              <?php foreach( $clients as $i => $client ) : ?>
                <?php if ( $i < $limit ) : ?>
                  <div class='<?= $block_name; ?>__item' data-index0='<?= $i; ?>'><?= $client; ?></div>
                <?php else : break; endif; ?>
              <?php endforeach; ?>
            </div>
            <div class="<?= $block_name; ?>__column">
              <?php foreach( $clients as $i => $client ) : ?>
                <?php if ( $i >= $limit && $i < $limit*2 ) : ?>
                  <div class='<?= $block_name; ?>__item' data-index0='<?= $i; ?>'><?= $client; ?></div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
            <div class="<?= $block_name; ?>__column">
              <?php foreach( $clients as $i => $client ) : ?>
                <?php if ( $i >= $limit*2 ) : ?>
                  <div class='<?= $block_name; ?>__item' data-index0='<?= $i; ?>'><?= $client; ?></div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
          </div>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
