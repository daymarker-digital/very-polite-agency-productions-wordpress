<?php

  /**
  *
  *   Featured Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'featured-text';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $heading = get_field('heading') ?: '';
  $heading_size = get_field('heading_size');
  $heading_style = get_field('heading_style');
  $layout_order = get_field('layout_order') ?: 'stacked';
  $layout_order_grid = "stacked" === $layout_order ? "one-column" : "two-column";
  $layout_text_alignment = get_field('layout_text_alignment') ?: 'left';
  $message = get_field('message') ?: '';



?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content grid grid--<?= $layout_order_grid; ?> grid--<?= $layout_order; ?> text--align-<?= $layout_text_alignment; ?>">

          <?php if ( $heading ) : ?>
            <?php
              $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]);
              $aos_delay += $aos_increment;
            ?>
            <h2 class="<?= $block_name; ?>__heading heading--<?= $heading_style; ?> heading--<?= $heading_size; ?>" <?= $aos_attrs; ?>><?= $heading; ?></h2>
          <?php endif; ?>
          <?php if ( $message ) : ?>
            <?php
              $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
            ?>
            <div class="<?= $block_name; ?>__message body-copy--primary body-copy--3" <?= $aos_attrs; ?>><?= $message; ?></div>
          <?php endif; ?>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
