<?php

  /**
  *
  *   Featured Images
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'featured-images';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Block Settings
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'full-width';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Block Data
  $images = get_field('images') ?: [];
  $images_aspect_ratio = get_field('images_aspect_ratio') ?: '';
  $layout_gutter = get_field('layout_gutter') ?: 0;
  $layout_grid = get_field('layout_grid') ?: 'one-column';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top')
      ]);
    ?>
    <?php if ( $images_aspect_ratio ) : ?>
      #<?= $block_id; ?> .grid__item.image {
        aspect-ratio: <?= $images_aspect_ratio; ?>
      }
    <?php endif; ?>
    <?php if ( $layout_gutter ) : ?>
      #<?= $block_id; ?> .grid {
        gap: <?= $layout_gutter; ?>px;
      }
    <?php endif; ?>

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
    <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
      <div class="<?= $block_name; ?>__main grid grid--<?= $layout_grid; ?>">

        <?php if ( !empty($images) ) : ?>
          <?php foreach( $images as $i => $item ) : ?>

            <?php
              $image = $item['image'] ?? [];
            ?>

            <?php if ( !empty($image) ) : ?>
              <?php
                $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]);
                $aos_delay += $aos_increment;
              ?>
              <div class="<?= $block_name; ?>__item grid__item image<?= $images_aspect_ratio ? ' with-ratio' : '' ?>" <?= $aos_attrs; ?>>
                <?= $THEME-> render_lazyload_image([ 'image' => $image ]); ?>
              </div>
            <?php endif; ?>

          <?php endforeach; ?>
        <?php endif; ?>

      </div>
    <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
  </section>

<?php endif; ?>
