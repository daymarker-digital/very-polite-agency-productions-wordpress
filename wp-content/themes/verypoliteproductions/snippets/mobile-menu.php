<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'mobile-menu';
$navigation = [
  [
    'title' => 'Services',
    'link' => '#services',
  ],
  [
    'title' => 'Studio',
    'link' => '#studio',
  ],
  [
    'title' => 'Pricing',
    'link' => '#pricing',
  ],
  [
    'title' => 'Contact',
    'link' => '#footer',
  ]
];


echo '<div class="' . $block_name . '" id="' . $block_name . '">';
  echo '<div class="' . $block_name . '__main">';

    echo $THEME->render_bs_container( 'open' );
      // echo $THEME->render_navigation();
    echo $THEME->render_bs_container( 'close' );

  echo '</div>';

  echo '<div class="' . $block_name . '__footer">';
    echo $THEME->render_bs_container( 'open' );
      echo '<div class="' . $block_name . '__contact">';
        // echo $THEME->render_contact_us();
      echo '</div>';
    echo $THEME->render_bs_container( 'close' );
  echo '</div>';

echo '</div>';

?>
