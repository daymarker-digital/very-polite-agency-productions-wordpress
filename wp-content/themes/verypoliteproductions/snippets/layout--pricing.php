<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$args = [
  'post_id' => get_the_ID(),
  'include_container' => true,
  'section_id' => get_sub_field( 'section_id' ),
  'disclaimer' => get_sub_field( 'disclaimer' ),
  'carousel' => get_sub_field( 'carousel' ),
  'heading' => 'Pricing',
  'image' => get_sub_field( 'image' ),
];

// echo $THEME->render( 'pricing', $args );

?>
