<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

?>

<div class="cookie-compliance">
  <div class="cookie-compliance__main">

    <p>This website uses cookies in order to heighten your experience. We will not use, collect or resell your personal information. You accept the use of cookies or other identifiers by closing or dismissing this notice, by clicking a link or button or by continuing to browse otherwise.</p>

    <button type="button">Ok</button>

  </div>
</div>
