<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$args = [
  'post_id' => get_the_ID(),
  'include_container' => true,
  'heading' => get_sub_field( 'heading' ),
];

// echo $THEME->render( 'heading', $args );

?>
