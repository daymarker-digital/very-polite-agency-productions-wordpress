<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$args = [
  'post_id' => get_the_ID(),
  'include_container' => true,
  'section_id' => get_sub_field( 'section_id' ),
  'heading' => get_sub_field( 'heading' ),
  'subheading' => get_sub_field( 'subheading' ),
  'message' => get_sub_field( 'message' ),
  'image' => get_sub_field( 'image' ),
];

// echo $THEME->render( 'image-feature', $args );

?>
