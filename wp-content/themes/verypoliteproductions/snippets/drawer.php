<?php

  /**
  *
  *	Filename: header.php
  *
  */

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'drawer';
  $template_id = $template;

  // ---------------------------------------- AOS
  $aos_id = $template_id;
  $aos_delay = 250;
  $aos_increment = 125;

  // ---------------------------------------- ACF Data
  $company_info = get_field( 'company_info', 'options' ) ?: [];
  $drawer_menu = get_field( 'drawer', 'options' ) ?: [];

  $address = $company_info;
  $social = $company_info['social'] ?? [];
  $email = $company_info['email'] ?? '';
  $phone = $company_info['phone'] ?? '';
  $navigation = $drawer_menu['navigation'] ?? [];

?>

<div class="<?= $template; ?> position--fixed z-index--200" id="<?= $template_id; ?>">

  <div class="<?= $template; ?>__main">
    <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>
      <div class="<?= $template; ?>__main-content">
        <?php if ( !empty($navigation) ) : ?>
          <?php
            $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
          ?>
          <nav class='<?= $template; ?>__navigation navigation navigation--header' role='navigation' aria-label='Drawer Menu Links' <?= $aos_attrs; ?>>
            <?php foreach ( $navigation as $i => $item ) : ?>
              <div class="<?= $template; ?>__navigation-item navigation__item">
                <?php
                  $link_type = $item['type'] ?? 'not-set';
                  $item['classes'] = "{$template}__navigation-link navigation__link link--{$link_type}";
                  $item['classes'] .= $link_type == 'external' ? " button--primary button--yellow" : " link"
                ?>
                <?= $THEME->render_nu_link( $item ); ?>
              </div>
            <?php endforeach; ?>
          </nav>
        <?php endif; ?>
      </div>
    <?= $THEME->render_bs_container( 'closed', 'col-12', 'container' ); ?>
  </div>

  <div class="<?= $template; ?>__footer">
    <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>
      <div class="<?= $template; ?>__footer-content body-copy--primary text--uppercase">

        <strong class="<?= $template; ?>__subheading">Very Polite Productions Ltd.</strong>

        <?php
          $address['style'] = 'human';
          $address_human = $THEME->get_google_maps_directions_link( $address );
          $address['style'] = 'directions';
          $address_link = $THEME->get_google_maps_directions_link( $address );
        ?>
        <div class="<?= $template; ?>__address">
          <a class="<?= $template; ?>__address-link link" href="<?= $address_link; ?>" target="_blank" title="Directions"><?= $address_human; ?></a>
        </div>

        <?php if ( $email ) : ?>
          <div class="<?= $template; ?>__email">
            <a class="<?= $template; ?>__email-link link" href="mailto:<?= $email; ?>" target="_blank" title="Email Us"><?= $email; ?></a>
          </div>
        <?php endif; ?>

        <?php if ( $phone ) : ?>
          <div class="<?= $template; ?>__phone">
            <a class="<?= $template; ?>__phone-link link" href="tel:<?= $phone; ?>" target="_blank" title="Phone Us"><?= $phone; ?></a>
          </div>
        <?php endif; ?>

        <div class="<?= $template; ?>__legal">
          <a class="<?= $template; ?>__legal-link link" href="https://weareverypolite.com" target="_blank" title="C/O Very Polite Agency, Inc.">C/O Very Polite Agency, Inc.</a>
        </div>

      </div>
    <?= $THEME->render_bs_container( 'closed', 'col-12', 'container' ); ?>
  </div>

</div>
