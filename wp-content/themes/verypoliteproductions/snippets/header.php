<?php

  /**
  *
  *	Filename: header.php
  *
  */

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'header';
  $template_id = $template;

  // ---------------------------------------- AOS
  $aos_id = $template_id;
  $aos_delay = 250;
  $aos_increment = 125;

  // ---------------------------------------- ACF Data
  $header = get_field( 'header', 'options' ) ?: [];
  $navigation = $header['navigation'] ?? [];

?>

<header class="<?= $template; ?> position--fixed z-index--250" id="<?= $template_id; ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>
    <div class="<?= $template; ?>__main">

      <div class="<?= $template; ?>__brand">
        <a class="<?= $template; ?>__brand-link link" href="<?= $THEME->get_theme_directory('home'); ?>" target="_self" title="Home">
          <?php
            $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]);
            $aos_delay += $aos_increment;
          ?>
          <span class="<?= $template; ?>__logo" <?= $aos_attrs; ?>><?= $THEME->render_svg_logo([ "type" => "main" ]); ?></span>
        </a>
      </div>

      <?php if ( !empty($navigation) ) : ?>
        <?php
          $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);
        ?>
        <nav class='<?= $template; ?>__navigation navigation navigation--header d-none d-lg-inline-flex' role='navigation' aria-label='Header Links' <?= $aos_attrs; ?>>
          <?php foreach ( $navigation as $i => $item ) : ?>
            <div class="<?= $template; ?>__navigation-item navigation__item">
              <?php
                $link_type = $item['type'] ?? 'not-set';
                $item['classes'] = "{$template}__navigation-link navigation__link link--{$link_type}";
                $item['classes'] .= $link_type == 'external' ? " button--primary button--yellow" : " link"
              ?>
              <?= $THEME->render_nu_link( $item ); ?>
            </div>
          <?php endforeach; ?>
        </nav>
      <?php endif; ?>

      <button class='<?= $template; ?>__button button--burger js--toggle-drawer d-lg-none' data-drawer-id='drawer' type='button'></button>

    </div>
  <?= $THEME->render_bs_container( 'closed', 'col-12', 'container' ); ?>
</header>
