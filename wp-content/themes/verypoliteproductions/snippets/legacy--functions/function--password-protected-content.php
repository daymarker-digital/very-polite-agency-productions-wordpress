<?php

//////////////////////////////////////////////////////////
////  Password Form
//////////////////////////////////////////////////////////

function custom_password_form() {
	
    global $post;
    
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$html = '';   
    
    $html .= '<form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">';
	    $html .= '<label class="pass-label" for="' . $label . '">' . __( "PASSWORD:" ) . ' </label>';
	    $html .= '<input name="post_password" id="' . $label . '" type="password" style="background: #ffffff; border:1px solid #999; color:#333333; padding:10px;" size="20" />';
	    $html .= '<input type="submit" name="Submit" class="button" value="' . esc_attr__( "Submit" ) . '" />';
	$html .= '</form>';
    
    return $html;
}

add_filter( 'the_password_form', 'custom_password_form' );

//////////////////////////////////////////////////////////
////  Title Trim
//////////////////////////////////////////////////////////

function the_title_trim ( $title ) {

	$title = esc_attr ( $title );

	$findthese = array(
		'#Protected:#',
		'#Private:#'
	);

	$replacewith = array(
		'', // What to replace "Protected:" with
		'' // What to replace "Private:" with
	);

	$title = preg_replace($findthese, $replacewith, $title);
	
	$trimmed = trim( $title );
	
	return $trimmed;
	
}

add_filter('the_title', 'the_title_trim');

?>
