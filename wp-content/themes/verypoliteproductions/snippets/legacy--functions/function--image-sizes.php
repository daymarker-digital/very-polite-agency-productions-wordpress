<?php
	
//////////////////////////////////////////////////////////
////  Custom Image Size Function
//////////////////////////////////////////////////////////

function VP_image_sizes() {

	// required 
	add_theme_support( 'post-thumbnails' );
	
	// initialize instance of PoliteDepartment
	$THEME = $THEME ?? new CustomTheme();
	
	// get custom sizes
	$custom_sizes = $THEME->custom_image_sizes;
	
	foreach ( $custom_sizes as $index => $size ) {
  	$size_title = $THEME->custom_image_title;
  	$size_title .= '-' . $size;
  	add_image_size( $size_title, $size, 9999 );
	}

} // VP_image_sizes()

add_action( 'after_setup_theme', 'VP_image_sizes' );

?>
