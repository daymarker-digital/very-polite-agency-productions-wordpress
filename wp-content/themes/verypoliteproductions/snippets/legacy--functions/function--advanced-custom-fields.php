<?php

//////////////////////////////////////////////////////////
////  ACF Master Options
//////////////////////////////////////////////////////////

if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page(
		array(
			'page_title' => 'Full Site Options',
			'menu_title' => 'Full Site Options',
			'menu_slug' => 'full-site-options',
			'capability' => 'edit_posts',
			'parent_slug' => '',
			'position' => false,
			'icon_url' => false
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Company Info',
			'menu_title' => 'Company Info',
			'menu_slug' => 'full-site-options-company',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Header',
			'menu_title' => 'Header',
			'menu_slug' => 'full-site-options-header',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Footer',
			'menu_title' => 'Footer',
			'menu_slug' => 'full-site-options-footer',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => '404 Page',
			'menu_title' => '404 Page',
			'menu_slug' => 'full-site-options-404',
			'capability' => 'edit_posts',
			'parent_slug' => 'full-site-options',
			'position' => false,
			'icon_url' => false
		)
	);

} // if function exists

function create_ACF_meta_in_REST() {

  $postypes_to_exclude = ['acf-field-group','acf-field'];
  $extra_postypes_to_include = ["page"];
  $post_types = array_diff(get_post_types(["_builtin" => false], 'names'),$postypes_to_exclude);
  array_push($post_types, $extra_postypes_to_include);
  foreach ($post_types as $post_type) {
    register_rest_field( $post_type, 'ACF', [
      'get_callback'    => 'expose_ACF_fields',
      'schema'          => null,
    ]);
  }

}

function expose_ACF_fields( $object ) {
  $ID = $object['id'];
  return get_fields($ID);
}

add_action( 'rest_api_init', 'create_ACF_meta_in_REST' );

?>
