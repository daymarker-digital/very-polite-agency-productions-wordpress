<?php

//////////////////////////////////////////////////////////
////  Register Custom Menus
//////////////////////////////////////////////////////////

function VeryPolite_custom_menus () {

	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'push-menu--left' => __( 'Push Menu [ Left ]' ),
			'push-menu--right' => __( 'Push Menu [ Right ]' )
		)
	);
	
}

add_action( 'init', 'VeryPolite_custom_menus' );

//////////////////////////////////////////////////////////
////  Print Menu
//////////////////////////////////////////////////////////

function print_menu ( $menu_title = 'Main Menu', $class_modifier = 'sample' ) {

	$menu_object = wp_get_nav_menu_items ( $menu_title );
	$current_page_id = get_the_ID();	
	$menu_items = array();

	if ( isset( $menu_object ) && !empty( $menu_object ) ) {

		//////////////////////////////////////////////////////////
		////  create formatted array of menu item data
		//////////////////////////////////////////////////////////
		
		foreach ( $menu_object as $menu_item ) {
			
			// defaults			
			$link = $link_title = $link_page_id = $link_parent_page_id = $link_active = false;

			if ( isset( $menu_item->url ) && !empty( $menu_item->url ) ) {
				$link = $menu_item->url;
			}

			if ( isset( $menu_item->title ) && !empty( $menu_item->title ) ) {
				$link_title = $menu_item->title;
			}

			if ( isset( $menu_item->object_id ) && !empty( $menu_item->object_id ) ) {
				$link_page_id = $menu_item->object_id;
				if ( $link_page_id == $current_page_id ) {
					$link_active = true;
				}
			}

			if ( isset( $menu_item->post_parent ) && !empty( $menu_item->post_parent ) ) {
				$link_parent_page_id = $menu_item->post_parent;
			}
			
			$temp = array( 
				'url' => $link,
				'title' => $link_title,
				'page_id' => $link_page_id,
				'parent_page_id' => $link_parent_page_id,
				'active' => $link_active
			);
			
			if ( ! $link_parent_page_id ) {
				
				$menu_items[$link_page_id] = array();
				
				array_push( $menu_items[$link_page_id], $temp );
				
			} else {

				array_push( $menu_items[$link_parent_page_id], $temp );

			}

		}
		
		//////////////////////////////////////////////////////////
		////  print formatted array of menu item data
		//////////////////////////////////////////////////////////

		echo '<ul class="menu menu--' . $class_modifier . '">';
		
		foreach ( $menu_items as $items ) {

			// default vars			
			$link = $link_title = $link_classes = $has_sub_items = false;

			// get the first item, top level menu item
			$top_level_item = array_shift( $items );

			if ( isset( $top_level_item['url'] ) && !empty( $top_level_item['url'] ) ) {
				$link = $top_level_item['url'];
			}

			if ( isset( $top_level_item['title'] ) && !empty( $top_level_item['title'] ) ) {
				$link_title = $top_level_item['title'];
			}

			if ( isset( $top_level_item['active'] ) && !empty( $top_level_item['active'] ) ) {
				$link_classes = 'active';
			}
			
			// get total number of remaining items
			if ( count( $items ) ) {
				$has_sub_items = true;
			}			

			echo '<li class="menu__item menu__item--' . $class_modifier . ( $link_classes ? ' ' . $link_classes : '' ) . '">';

				echo '<a href="' . $link . '">' . $link_title . ( $has_sub_items ? '<div class="arrow arrow--up"><img src="' . $active_state_icon . '" /></div>' : '' ) . '</a>';

				if ( !empty( $items ) ) {

					echo '<ul class="sub-menu sub-menu--' . $class_modifier . '">';

						foreach ( $items as $item ) {

							// default vars			
							$link = $link_title = $link_classes = false;

							if ( isset( $item['url'] ) && !empty( $item['url'] ) ) {
								$link = $item['url'];
							}
							if ( isset( $item['title'] ) && !empty( $item['title'] ) ) {
								$link_title = $item['title'];
							}
							if ( isset( $item['active'] ) && !empty( $item['active'] ) ) {
								$link_classes = 'active';
							}
							
							echo '<li class="sub-menu__item sub-menu__item--' . $class_modifier . ( $link_classes ? ' ' . $link_classes : '' ) . '">';
								echo '<a href="' . $link . '">' . $link_title . '</a>';
							echo '</li>';
							
						}
					
					echo '</ul>';	

				}
					
			echo '</li>';
						
		}
		
		echo '</ul>';	
				
		/*			
										
		foreach ( $menu_object as $menu_item ) {
			
			// debug_this( $menu_item );
			
			// [ID] => 24
			// [menu_item_parent] => 24
																								
			$link_title = $link = $link_id = false;
			$link_classes = '';
			$link_icon = false;
			
			if ( isset( $menu_item->url ) && !empty( $menu_item->url ) ) {
				$link = $menu_item->url;
			}
			
			if ( isset( $menu_item->title ) && !empty( $menu_item->title ) ) {
				$link_title = $menu_item->title;
			}
			
			if ( isset( $menu_item->object_id ) && !empty( $menu_item->object_id ) ) {
				$link_id = $menu_item->object_id;
				if ( $link_id == $current_post_id ) {
					$link_classes = 'active';
				}
				if ( get_field( 'icon', $link_id ) ) {
					$link_icon = get_field( 'icon', $link_id );
					$link_icon = $link_icon['url'];
				}
			}
										
			if ( $link_title && $link ) {
				echo '<li class="menu__item menu__item--' . $class_modifier . ' ' . $link_classes . '">';
				
					echo '<a href="' . $link . '">';
						echo $link_title;						
					echo '</a>';
					
				echo '</li>';
			}
			
		} // foreach $menu_object as $menu_item
					
		
		
		*/
					
	} // if $menu_object

}

?>
