<script async src="https://cdn.jsdelivr.net/npm/lazysizes@5.3.2/lazysizes.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/axios@1.3.3/dist/axios.min.js"></script>

<script>

  window.lazySizesConfig = window.lazySizesConfig || {};
  window.lazySizesConfig.lazyClass = 'lazyload';
  lazySizesConfig.loadMode = 1;

  document.addEventListener('lazybeforeunveil', function(e){
    let bg = e.target.getAttribute('data-bg');
    if ( bg ) e.target.style.backgroundImage = 'url(' + bg + ')';
  });

  document.addEventListener('lazyloaded', function(e){
    e.target.parentNode.classList.add('lazyloaded');
  });

</script>
