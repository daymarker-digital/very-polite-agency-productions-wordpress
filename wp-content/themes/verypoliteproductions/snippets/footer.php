<?php

  /**
  *
  *	Filename: footer.php
  *
  */

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'footer';
  $template_id = $template;

  // ---------------------------------------- AOS
  $aos_id = $template_id;
  $aos_delay = 250;
  $aos_increment = 125;

  // ---------------------------------------- ACF Data
  $company_info = get_field( 'company_info', 'options' ) ?: [];
  $address = $company_info;
  $social = $company_info['social'] ?? [];
  $email = $company_info['email'] ?? '';
  $phone = $company_info['phone'] ?? '';

?>

<footer class="<?= $template; ?>" id="<?= $template_id; ?>">
  <div class="<?= $template; ?>__main">
    <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>
      <div class="<?= $template; ?>__main-content text--align-center text--uppercase">

        <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 0, 'offset' => 0, 'transition' => 'fade-down' ]); ?>
        <h2 class="<?= $template; ?>__heading heading--primary heading--lg" <?= $aos_attrs; ?>>Contact</h2>

        <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 250, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
        <strong class="<?= $template; ?>__subheading body-copy--primary body-copy--3 text--uppercase" <?= $aos_attrs; ?>>Very Polite Productions Ltd.</strong>

        <?php
          $address['style'] = 'human';
          $address_human = $THEME->get_google_maps_directions_link( $address );
          $address['style'] = 'directions';
          $address_link = $THEME->get_google_maps_directions_link( $address );
          $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 350, 'offset' => 0, 'transition' => 'fade-left' ]);
        ?>
        <div class="<?= $template; ?>__address body-copy--primary body-copy--3" <?= $aos_attrs; ?>>
          <a class="<?= $template; ?>__address-link link" href="<?= $address_link; ?>" target="_blank" title="Directions"><?= $address_human; ?></a>
        </div>

        <?php if ( $email ) : ?>
          <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 450, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
          <div class="<?= $template; ?>__email body-copy--primary body-copy--3" <?= $aos_attrs; ?>>
            <a class="<?= $template; ?>__email-link link" href="mailto:<?= $email; ?>" target="_blank" title="Email Us"><?= $email; ?></a>
          </div>
        <?php endif; ?>

        <?php if ( $phone ) : ?>
          <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 550, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
          <div class="<?= $template; ?>__phone body-copy--primary body-copy--3"<?= $aos_attrs; ?>>
            <a class="<?= $template; ?>__phone-link link" href="tel:<?= $phone; ?>" target="_blank" title="Phone Us"><?= $phone; ?></a>
          </div>
        <?php endif; ?>

        <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 650, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
        <div class="<?= $template; ?>__icon-set" <?= $aos_attrs; ?>>
          <?= $THEME->render_svg_icon([ "type" => "set" ]); ?>
        </div>

        <?php $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => 750, 'offset' => 0, 'transition' => 'fade-left' ]); ?>
        <div class="<?= $template; ?>__legal body-copy--primary body-copy--3" <?= $aos_attrs; ?>>
          <a class="<?= $template; ?>__legal-link link" href="https://weareverypolite.com" target="_blank" title="C/O Very Polite Agency, Inc.">C/O Very Polite Agency, Inc.</a>
        </div>

        <?php
          if ( !empty($social) ) {
            echo "<nav class='{$template}__social' role='navigation' aria-label='Social Links'>";
              foreach ( $social as $i => $item ) {

                $type = $item['type'] ?? 'not-set';
                $link = $item['link'] ?? '';
                $link_target = '_blank';
                $link_title = get_bloginfo('name') . " " . ucfirst($type);
                $link = 'email' === $type ? 'mailto:' . $link : $link;
                $aos_delay = (1000 + ( $i * 150 ));
                $aos_attrs = $THEME->render_aos_attributes([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'offset' => 0, 'transition' => 'fade-up' ]);

                echo $type && $link ? "
                  <div class='{$template}__social-item {$type}' {$aos_attrs}>
                    <a class='{$template}__social-link link' href='{$link}' target='{$link_target}' title='{$link_title}'>{$THEME->render_svg_icon([ "type" => $type ])}</a>
                  </div>
                " : "";

              }
            echo "</nav>";
          }
        ?>

      </div>
    <?= $THEME->render_bs_container( 'closed', 'col-12', 'container' ); ?>
  </div>
</footer>
