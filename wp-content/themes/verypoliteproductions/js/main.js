import Credits from './modules/credits';
import Drawers from './modules/drawers';
import Gliders from './modules/gliders';
import Tools from './modules/tools';

AOS.init({
  offset: 150,                // offset (in px) from the original trigger point
  delay: 0,                   // values from 0 to 3000, with step 50ms
  duration: 500,              // values from 0 to 3000, with step 50ms
  easing: 'ease-in-out',      // default easing for AOS animations
});

Credits.init();
Drawers.init();

window.addEventListener( 'load', function (e) {
  Tools.setElementsHeightToCSSVariable();
  AOS.refresh();
  Gliders.init();
});

window.addEventListener( 'resize', Tools.debounce(() => {
  Tools.setElementsHeightToCSSVariable();
}, 300));

window.addEventListener( 'resize', Tools.throttle(() => {}, 300));

window.addEventListener( 'scroll', Tools.debounce(() => {}, 300));

window.addEventListener( 'scroll', Tools.throttle(() => {}, 300));
