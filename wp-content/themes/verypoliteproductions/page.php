<?php

  /**
  *
  *	Template Name: Page
  *	Filename: page.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'index';
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  // ---------------------------------------- Template
  echo '<div class="' . $template . '" id="' . $template_id . '">';

    if ( have_posts() ) {
	    while ( have_posts() ) {

		    // init post data
		    the_post();

		    // default data
        the_content();

	    }
    }

  echo '</div>';

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
