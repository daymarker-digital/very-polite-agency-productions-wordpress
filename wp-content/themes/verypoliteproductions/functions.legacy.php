<?php

/*
*
*	Filename: functions.php
*
*/

class PoliteDepartment {

  //////////////////////////////////////////////////////////
  ////  Vars
  //////////////////////////////////////////////////////////

  private $version = 1.0;

  public $custom_image_title = "custom-image-size";
  public $custom_image_sizes = array( 1, 10, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 );

  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////

  public function __construct () {}

  //////////////////////////////////////////////////////////
  ////  Theme Classes
  //////////////////////////////////////////////////////////

  public function theme_classes () {

    global $post;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$post_template = basename( get_page_template( $post_ID ), ".php" );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--'. $post_template;
  		$classes .= ' '. $post_template;

      if ( is_front_page() ) {
        $classes .= ' is-front-page';
      }

      if ( is_page() ) {
        $classes .= ' is-page';
      }

      if ( is_page_template( 'page-templates/page--about-us.php') ) {
        $classes .= ' is-about-us-page';
      }

      if ( is_single() ) {
        $classes .= ' is-single';
      }

      if ( is_category() ) {
        $classes .= ' is-category';
      }

  	}

    if ( is_404() ) {
      $classes .= ' is-404';
    }

  	return $classes;

  }

  //////////////////////////////////////////////////////////
  ////  Theme Info
  //////////////////////////////////////////////////////////

  public function theme_info ( $param = 'version' ) {

    global $post;

    switch ( $param ) {

      case 'version':
				return $this->version;
				break;
			case 'post_ID':
				return get_the_ID();
				break;
			case 'post_type':
				return get_post_type( get_the_ID() );
				break;
			case 'template':
				return basename( get_page_template(), ".php" );
				break;
			case 'wp_version':
				return get_bloginfo( "version" );
				break;
			case 'php_version':
				return phpversion();
				break;
    }

  }

  //////////////////////////////////////////////////////////
  ////  Theme Directory
  //////////////////////////////////////////////////////////

  public function theme_directory ( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  //////////////////////////////////////////////////////////
  ////  Get Featured Image
  //////////////////////////////////////////////////////////

  public function get_featured_image ( $post_id = false ) {

    $image = $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  //////////////////////////////////////////////////////////
  ////  Lazyload Image
  //////////////////////////////////////////////////////////

  public function lazyload_image ( $media = false, $params = [] ) {

    // default data
    $html = '';
    $classes = 'lazyload lazyload-item lazyload-item--image';
    $is_gif = $is_svg = false;
    $sizes_title = $this->custom_image_title;
    $sizes = $this->custom_image_sizes;

    // default params
    $alt_text = $background = false;
    $preload = $responsive = true;
    $delay = 50;
    $duration = 500;

    // conditionally update media settings
    if ( $media ) {

      // deconstruct $params object
      extract( $params );

      // check media object to see if mime type is SVG
      if ( isset( $media['subtype'] ) ) {
        $is_svg = ( 'svg+xml' === $media['subtype'] ) ? true : false;
        $is_gif = ( 'gif' === $media['subtype'] ) ? true : false;
      }

      // conditionally check if alt text is not provided argument and is provided with media
      if ( !$alt_text && isset( $media['alt'] ) && !empty( $media['alt'] ) ) {
        $alt_text = $media['alt'];
      }

      // conditionally build classes
      if ( $background ) {
        $classes .= ' lazyload-item--background';
      } else {
        $classes .= ' lazyload-item--inline';
      }
      if ( $preload ) {
        $classes .= ' lazypreload';
      }

      // conditionally build html
      if ( $background ) {

        $html = '<div
          class="' . $classes . '"
          data-bg="' . $media['url'] . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"
          style="background-url(' . $media['sizes'][$sizes_title . '-1'] . ');"
          ></div>';

       } else {

        $html = '<img
          class="' . $classes . '"
          width="' . $media['width'] . '"
          height="' . $media['height'] . '"
          data-src="' . $media['url'] . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"';

        if ( !$is_svg ) {

          $html .= ' src="' . $media['sizes'][$sizes_title . '-10'] . '"';

          if ( $responsive && !$is_gif ) {

            $srcset = '';

            foreach ( $sizes as $index => $size ) {
              if ( 0 == $index ) {
                $srcset .= $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
              } else {
                $srcset .= ', ' . $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
              }
            }

            $html .= ' data-sizes="auto"';
            $html .= ' data-srcset="' . $srcset . '"';

          }

        }

        if ( $alt_text ) {
          $html .= ' alt="' . $alt_text . '"';
        }

        $html .= ' />';

      }

    }

    return $html;

  }

  //////////////////////////////////////////////////////////
  ////  Lazyload Video
  //////////////////////////////////////////////////////////

  public function lazyload_video ( $media = false, $params = [] ) {

    // default data
    $html = '';
    $classes = 'lazyload lazyload-item lazyload-item--video';
    $url = $mime_type = false;

    // default params
    $alt_text = $background = false;
    $preload = $responsive = true;
    $delay = 50;
    $duration = 500;

    // conditionally update media settings
    if ( $media ) {

      // deconstruct $params
      extract( $params );

      // conditionally build classes
      if ( $background ) {
        $classes .= ' lazyload-item--background';
      } else {
        $classes .= ' lazyload-item--inline';
      }
      if ( $preload ) {
        $classes .= ' lazypreload';
      }

      if ( isset($media['url']) && !empty($media['url']) ) {
        $url = $media['url'];
      }
      if ( isset($media['mime_type']) && !empty($media['mime_type']) ) {
        $mime_type = $media['mime_type'];
      }

      if ( $url && $mime_type ) {
        $html .= '<video
          class="' . $classes . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"
          preload="none"
          muted=""
          data-autoplay=""
          loop playsinline muted
        >';
          $html .= '<source src="' . $media['url'] . '" type="' . $media['mime_type'] . '">';
        $html .= '</video>';
      }

    }

    return $html;

  }

  //////////////////////////////////////////////////////////
  ////  Lazyload iFrame
  //////////////////////////////////////////////////////////

  public function lazyload_iframe ( $id = false, $params = [] ) {

    // default data
    $html = '';
    $url = '';
    $classes = 'lazyload lazyload-item lazyload-item--iframe';

    // default params
    $background = $source = false;
    $preload = $responsive = true;
    $delay = 50;
    $duration = 500;

    // conditionally update media settings
    if ( $id ) {

      // deconstruct $params
      extract( $params );

      if ( $preload ) {
        $classes .= ' lazypreload';
      }

      switch ( $source ) {
        case 'vimeo':
          $url = 'https://player.vimeo.com/video/';
          break;
        case 'youtube':
          $url = 'https://www.youtube.com/embed/';
          break;
      }

      if ( $url && $id ) {
        $html = '<iframe
          class="' . $classes . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          data-src="' . $url . $id . '"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>';
      }

    }

    return $html;

  }

}

//////////////////////////////////////////////////////////
////  Templates
//////////////////////////////////////////////////////////

include_once( 'templates.php' );

//////////////////////////////////////////////////////////
////  Utilities
//////////////////////////////////////////////////////////

include_once( 'snippets/function--utilities.php' );

//////////////////////////////////////////////////////////
////  Security
//////////////////////////////////////////////////////////

include_once( 'snippets/function--security.php' );

//////////////////////////////////////////////////////////
////  ACF Master Options
//////////////////////////////////////////////////////////

include_once( 'snippets/function--advanced-custom-fields.php' );

//////////////////////////////////////////////////////////
////  Menus
//////////////////////////////////////////////////////////

include_once( 'snippets/function--menus.php' );

//////////////////////////////////////////////////////////
////  Image Sizes
//////////////////////////////////////////////////////////

include_once( 'snippets/function--image-sizes.php' );

//////////////////////////////////////////////////////////
////  Custom Post Types
//////////////////////////////////////////////////////////

include_once( 'snippets/function--custom-post-types.php' );

//////////////////////////////////////////////////////////
////  Password Form
//////////////////////////////////////////////////////////

include_once( 'snippets/function--password-protected-content.php' );

//////////////////////////////////////////////////////////
////  Google Maps Link Builder
//////////////////////////////////////////////////////////

include_once( 'snippets/function--google-maps-link-builder.php' );

//////////////////////////////////////////////////////////
////  Custom WYSIWYG Formats
//////////////////////////////////////////////////////////

include_once( 'snippets/function--custom-wysiwyg-formats.php' );

//////////////////////////////////////////////////////////
////  Optimization
//////////////////////////////////////////////////////////

include_once( 'snippets/function--optimization.php' );

//////////////////////////////////////////////////////////
////  Enqueue Scripts & Styles
//////////////////////////////////////////////////////////

include_once( 'snippets/function--enqueue-scripts-styles.php' );
