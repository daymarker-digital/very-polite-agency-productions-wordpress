<?php
	
/*
*		
*	Filename: single.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
 
if ( have_posts() ) {
	while ( have_posts() ) {
		
		// init post data
		the_post();
		
		// default data
    $title = false;
		
		// get data
    if ( get_the_title() ) {
      $title = get_the_title();
    }
    
    if ( $title ) {
      
      echo '<article>';
        echo '<h1>' . $title . '</h1>';
      echo '</article>';
      
    }	
	
	}
}

get_footer(); 

?>
