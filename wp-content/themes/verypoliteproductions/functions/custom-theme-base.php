<?php

class CustomThemeBase {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Custom Theme Base';
  private $version = '2.0';

  public $custom_image_title = 'custom-image-size';
  public $custom_image_sizes = [ 1, 10, 90, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];

  /*
  //////////////////////////////////////////////////////////
  ////  Methods
  //////////////////////////////////////////////////////////
  */

  // --------------------------- Get Acronym from Words
  public function get_acronym_from_words( $words = '' ) {

    $acronym = '';

    $words = preg_split("/[\s,_-]+/", $words );

    if ( $words ) {
      foreach ( $words as $i => $word ) {
        $acronym .= $word[0];
      }
    }

    return $acronym;

  }

  // --------------------------- Featured Image
  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = [];
    $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  // --------------------------- Google Maps Directions Link
  public function get_google_maps_directions_link( $params = [] ) {

    extract(array_merge(
      [
        'address' => '',
        'address_2' => '',
        'base' => 'https://www.google.com/maps/search/?api=1&query=',
        'city' => '',
        'country' => '',
        'html' => '',
        'name' => get_bloginfo('name'),
        'region' => '',
        'postal' => '',
        'style' => 'directions'
      ],
      $params
    ));

    $html .= $address ?? "";
    $html .= $city ? "<br>{$city}" : "";
    $html .= $region ? " {$region}" : "";
    $html .= $postal ? " {$postal}" : "";
    $html = $address_2 ? "{$address_2} – {$html}" : $html;

    switch ( $style ) {
      case 'directions': {
        $html = $name ? "{$name}, {$html}" : $html;
        $html .= $country ? " {$country}" : "";
        $html = $base . rawurlencode(str_replace("<br>", " ", $html));
        break;
      }
    }

    return $html;

  }

  // --------------------------- Theme Classes
  public function get_theme_classes() {

    global $post;
    global $template;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$template = basename( $template, '.php' );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--' . $template;
  		$classes .= ' ' . $template;

      if ( is_front_page() ) {
        $classes .= ' is-front-page';
      }

      if ( is_page() ) {
        $classes .= ' is-page';
      }

      if ( is_page_template( 'page-templates/page--about-us.php') ) {
        $classes .= ' is-about-us-page';
      }

      if ( is_single() ) {
        $classes .= ' is-single';
      }

      if ( is_archive() ) {
        $classes .= ' is-archive';
      }

      if ( is_category() ) {
        $classes .= ' is-category';
      }

  	}

    if ( is_404() ) {
      $classes .= ' is-404';
    }

  	return trim($classes);

  }

  // --------------------------- Theme Directory
  public function get_theme_directory( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  // --------------------------- Theme Info
  public function get_theme_info( $param = 'version' ) {

    global $post;

    switch ( $param ) {
      case 'network_domain':
				$html = is_multisite() ? get_blog_details()->domain : 'Not Multisite!';
				break;
      case 'network_id':
				$html = is_multisite() ? get_blog_details()->blog_id : 'Not Multisite!';
				break;
      case 'network_name':
				$html = is_multisite() ? get_blog_details()->blogname : 'Not Multisite!';
				break;
      case 'object_ID':
				$html = get_queried_object_id();
				break;
      case 'php_version':
				$html = phpversion();
				break;
      case 'post_ID':
				$html = ( $post ) ? $post->ID : 'no-post-id';
				break;
			case 'post_type':
				$html = get_post_type( $post->ID );
				break;
      case 'template':
				$html = basename( get_page_template(), ".php" );
				break;
      case 'version':
				$html = $this->version;
				break;
      case 'vitals':
        $html = "<!-- PHP Version: " . $this->get_theme_info( 'php_version' ) . " -->
  	      <!-- WP Version: " . $this->get_theme_info( 'wp_version' ) . " -->
  	      <!-- Current Template: " . $this->get_theme_info( 'template' ) . " -->
  	      <!-- Post ID: " . $this->get_theme_info( 'post_ID' ) . " -->
          <!-- Network Domain: " . $this->get_theme_info( 'network_domain' ) . " -->
          <!-- Network ID: " . $this->get_theme_info( 'network_id' ) . " -->
          <!-- Network Name: " . $this->get_theme_info( 'network_name' ) . " -->
        ";
				break;
      case 'wp_version':
				$html = get_bloginfo( "version" );
				break;
      default:
        $html = '';
        break;
    }

    return $html;

  }

  // --------------------------- Get Unique ID
  public function get_unique_id( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'prefix' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $html .= $prefix ? $prefix . '-' . md5(uniqid(rand(), true)) : md5(uniqid(rand(), true));

    // ---------------------------------------- Template
    return $html;

  }

  // --------------------------- Anchor
  public function render_anchor( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [ 'block_name' => '', 'id' => '' ],
      $params
    ));

    return $block_name && $id ? "<div class='{$block_name}__anchor anchor' id='{$id}'></div>" : "";

  }

  // --------------------------- AOS Attributes
  public function render_aos_attributes( $params = [] ) {

    /*
    *  Note:
    *  AOS library (https://www.npmjs.com/package/aos) needs to be installed
    *  and initialized. JS and CSS files are required for anything to happen.
    *
    *  Timing:
    *  Both delay and duration must be increments of 50
    *
    *  Bugs:
    *  Offset and Mirror are buggy. Disable them for now.
    */

    // ---------------------------------------- Defaults
    $settings = array_merge([
        'anchor' => '',                           // element id
        'anchor_placement' => 'top-bottom',
        'delay' => 0,
        'duration' => 650,
        'easing' => 'ease-in-out',
        // 'mirror' => 'false',                   // DEF buggy, hide for now
        'offset' => 225,                          // buggy, so hide for now (...or?)
        'once' => 'true',
        'transition' => 'fade-in',
      ], $params
    );

    $html = '';

    foreach ( $settings as $key => $value ) {
      switch ( $key ) {
        case 'anchor': {
          $html .= ' data-aos-anchor="#' . $value . '"';
          break;
        }
        case 'anchor_placement': {
          $html .= ' data-aos-anchor-placement="' . $value . '"';
          break;
        }
        case 'transition': {
          $html .= ' data-aos="' . $value . '"';
          break;
        }
        default:
          $html .= ' data-aos-' . $key . '="' . $value . '"';
      }
    }

    return $html;

  }

  // --------------------------- Container
  public function render_bs_container( $state = 'open', $col = 'col-12', $container = 'container' ) {
    if ( "full-width" !== $container ) {
      return "open" !== $state ? "</div></div></div>" : "<div class='{$container}'><div class='row'><div class='{$col}'>";
    }
    return;
  }

  // --------------------------- Call to Action
  public function render_cta( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'cta',
        'classes' => '',
        'html' => '',
        'link' => '',
        'link_block_name' => 'link',
        'link_category' => '',
        'link_classes' => '',
        'link_external' => '',
        'link_page' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
        'type' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;

    switch( $type ) {
      case 'category': {
        $link = get_category_link( $link_category ) ?: '';
        break;
      }
      case 'external': {
        if ( $link_external ) {
          $link = $link_external;
          $rel = 'noopener';
          $target = '_blank';
        }
        break;
      }
      case 'page': {
        $link = get_permalink( $link_page ) ?: '';
        break;
      }
    }

    if ( $link && $title ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $this->render_link([
          'block_name' => $link_block_name,
          'classes' => $link_classes,
          'link' => $link,
          'title' => $title,
          'style' => $style,
          'target' => $target,
          'rel' => $rel
        ]);
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Element Styles
  public function render_element_styles( $params = [] ) {

    extract(array_merge(
      [
        'id' => '',
        'padding_bottom' => 0,
        'padding_top' => 0,
      ],
      $params
    ));

    if ( $id ) {
      return "
        #{$id} {
          padding-top: calc({$padding_top}px * 0.75);
          padding-bottom: calc({$padding_bottom}px  * 0.75);
        }
        @media screen and (min-width: 992px) {
          #{$id} {
            padding-top: {$padding_top}px;
            padding-bottom: {$padding_bottom}px;
          }
        }
      ";
    }

  }

  // ---------------------------------------- Form Error Message
  public function render_form_error_message( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__error-message error-message',
        'modifier' => '',
        'type' => 'default',
      ],
      $params
    ));

    $classes .= $modifier ? $classes . ' error-message--' . $modifier : '';
    $messages = [
      'checkbox' => 'This field is required',
      'default' => 'This field is required',
      'email' => 'Please enter a valid email address',
      'file' => 'File type or file size incorrect',
      'tel' => 'Please enter a valid phone number',
      'select' => 'Please select a valid option',
      'text' => 'This field cannot be blank',
      'textarea' => 'This field cannot be blank',
    ];

    return '<div class="' . $classes . '">' . ( $messages[$type] ?? '' ) . '</div>';

  }

    // ---------------------------------------- Form Field
  public function render_form_field( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accept' => '',
        'block_name' => 'field',
        'checkbox_options' => [],
        'disabled' => false,
        'hidden' => false,
        'honeypot' => false,
        'html' => '',
        'id' => '',
        'label' => '',
        'label_hidden' => false,
        'multiple' => false,
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'select_default' => '',
        'select_options' => [],
        'selected_value' => '',
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    $block_classes = 'form__field field field--' . $type;
    $block_classes .= $honeypot ? ' field--honeypot' : '';

    if ( $name && $type ) {
      $html .= '<div class="' . $block_classes . '">';

        if ( 'checkbox' !== $type ) {
          $html .= $this->render_form_label([
            'for' => $id,
            'hidden' => $label_hidden,
            'required' => $required,
            'value' => $label,
          ]);
        }

        switch ( $type ) {

          // ---------------------------------------- Checkbox
          case 'checkbox': {

            if ( $label && $name && $value ) {
              $html .= '<div class="checkbox">';
                $html .= '<label ' . ( $id ? 'for="' . $id . '"' : '' ) . '>';
                  $html .= '<div class="checkbox__label">' . $label . '</div>';
                  $html .= '<input
                    class="checkbox__input' . ( $required ? ' required' : '' ) . '"
                    type="' . $type . '"
                    name="' . $name . '"
                    value="' . $value . '"
                    ' . ( $id ? 'id="' . $id . '"' : '' ) . '
                  />';
                  $html .= '<div class="checkbox__content">';
                    $html .= '<div class="checkbox__value">' . $value . '</div>';
                  $html .= '</div>';
                $html .= '</label>';
              $html .= '</div>';
            }

            break;

          }

          // ---------------------------------------- Standard
          case 'email':
          case 'file':
          case 'tel':
          case 'text': {

            $html .= $this->render_form_input([
              'accept' => $accept,
              'disabled' => $disabled,
              'honeypot' => $honeypot,
              'hidden' => $hidden,
              'id' => $id,
              'multiple' => $multiple,
              'name' => $name,
              'placeholder' => $placeholder,
              'readonly' => $readonly,
              'required' => $required,
              'type' => $type,
              'value' => $value,
            ]);

            break;

          }

          // ---------------------------------------- Select
          case 'select': {

            $html .= $this->render_form_select([
              'default' => $select_default,
              'disabled' => $disabled,
              'hidden' => $hidden,
              'id' => $id,
              'multiple' => $multiple,
              'name' => $name,
              'readonly' => $readonly,
              'required' => $required,
              'selected' => $selected_value,
              'options' => $select_options,
            ]);

            break;

          }

          // ---------------------------------------- Textarea
          case 'textarea': {

            $html .= $this->render_form_textarea([
              'disabled' => $disabled,
              'hidden' => $hidden,
              'id' => $id,
              'name' => $name,
              'placeholder' => $placeholder,
              'readonly' => $readonly,
              'required' => $required,
              'type' => $type,
              'value' => $value,
            ]);

            break;

          }

        }

        if ( $required ) {
          $html .= $this->render_form_error_message([
            'type' => $type
          ]);
        }

      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Form Input
  public function render_form_input( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accept' => '',
        'classes' => 'form__input input',
        'disabled' => false,
        'honeypot' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'multiple' => false,
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= ' input--' . $type;
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<input
        class="' . $classes . '"
        ' . ( $accept ? 'accept="' . $accept . '"' : '' ) . '
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $honeypot ? 'tabindex="-1"' : '' ) . '
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $multiple ? 'multiple' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $type === 'email' ? 'spellcheck="false" autocapitalize="off"' : '' ) . '
        ' . ( $type === 'tel' ? 'pattern="[0-9\-]*"' : '' ) . '
        type="' . $type . '"
        name="' . $name . '"
        autocomplete="' . $type . '"
      >';

    }

    return $html;

  }

  // ---------------------------------------- Form Label
  public function render_form_label( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__label label',
        'for' => '',
        'hidden' => false,
        'html' => '',
        'modifier' => '',
        'required' => false,
        'value' => '',
      ],
      $params
    ));

    $classes .= $modifier ? $classes . ' label--' . $modifier : '';
    $classes .= $hidden ? ' d-none' : '';
    $value .= $required ? '<span class="required-marker">*</span>' : '';

    if ( $value ) {
      $html .= '<label
        class="' . $classes . '"
        ' . ( $for ? 'for="' . $for . '"' : '' ) . '
      >' . $value . '</label>';
    }

    return $html;

  }

  // ---------------------------------------- Form Select
  public function render_form_select( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__select select',
        'default' => '',
        'disabled' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'modifier' => '',
        'multiple' => false,
        'name' => '',
        'readonly' => false,
        'required' => false,
        'selected' => '',
        'options' => [],
      ],
      $params
    ));

    if ( $name && $options ) {

      $classes .= $modifier ? $classes . ' select--' . $modifier : '';
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<select
        class="' . $classes . '"
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $multiple ? 'multiple' : '' ) . '
        name="' . $name . '"
      >';
      $html .= $default ? '<option value="">' . $default . '</option>' : '';
      foreach ( $options as $index => $option ) {
        $title = $option['title'] ?? '';
        $value = $option['value'] ?? '';
        if ( $title && $value ) {
          $html .= '<option value="' . $value . '" ' . ( $selected === $value ? 'selected' : '' ) . '>' . $title . '</option>';
        }
      }
      $html .= '</select>';

    }

    return $html;

  }

  // ---------------------------------------- Form Textarea
  public function render_form_textarea( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__textarea textarea',
        'disabled' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'modifier' => '',
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= $modifier ? $classes . ' textarea--' . $modifier : '';
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<textarea
        ' . ( $classes ? 'class="' . $classes . '"' : '' ) . '
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        name="' . $name . '"
      ></textarea>';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload iFrame
  public function render_lazyload_iframe( $params = [] ) {

    $defaults = [
      'aspect_ratio' => '16-9',
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'preload' => false,
      'video_id' => '', // 160730254, 163590531, 221632885
      'video_source' => 'vimeo',
    ];

    extract( array_merge( $defaults, $params ) );

    $iframe_classes = 'lazyload lazyload-item lazyload-item--iframe';
    $iframe_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $iframe_classes .= $preload ? ' lazypreload' : '';
    $iframe_classes = $classes ? $classes . ' ' . $iframe_classes : $iframe_classes;

    $video_source_url = ( 'vimeo' == $video_source ) ? 'https://player.vimeo.com/video/' : 'https://www.youtube.com/embed/';
    $video_source_url .= $video_id;
    $video_source_url .= ( $background ) ? '?autoplay=1&loop=1&autopause=0&muted=1&background=1' : '?autoplay=0&loop=1&autopause=0&muted=0&background=0';

    if ( $video_source_url && $video_id ) {

      $html = '
        <iframe
          class="' . trim($iframe_classes) . '"
          data-aspect-ratio="' . $aspect_ratio . '"
          data-src="' . $video_source_url . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_lazyload_image( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'alt_text' => '',
        'classes' => '',
        'custom_sizes_title' => $this->custom_image_title,
        'custom_sizes' => $this->custom_image_sizes,
        'delay' => 0,
        'duration' => 300,
        'html' => '',
        'image' => [],
      ],
      $params
    ));

    $base_classes = 'lazyload lazyload-item lazyload-item--image lazypreload';
    $classes = ( $classes ?? '' ) . ' ' . $base_classes;
    $img_attrs = '';
    $img_srcset = '';

    if ( !empty($image) ) {

      $img_type = $image['subtype'] ?? 'no-set';
      $img_src = $image['url'] ?? '';

      $img_attrs .= 'class="' . trim($classes) . '"';
      $img_attrs .= $image['width'] ? ' width="' . $image['width'] . '"' : '';
      $img_attrs .= $image['height'] ? ' height="' . $image['height'] . '"' : '';
      $img_attrs .= ' alt="' . ( $alt_text ?: ( $image['alt'] ?? get_bloginfo('name') . ' Photography' ) ) . '"';
      $img_attrs .= $img_src ? ' data-src="' . $img_src . '"' : '';
      $img_attrs .= ' data-transition-delay="' . $delay . '"';
      $img_attrs .= ' data-transition-duration="' . $duration . '"';

      switch ( $img_type ) {
        case 'svg+xml': {
          $img_attrs .= $img_src ? ' src="' . $img_src . '"' : '';
          break;
        }
        default: {
          if ( isset($image['sizes']) && !empty($image['sizes']) ) {
            foreach ( $custom_sizes as $i => $size ) {
              $img_srcset .= ( $i > 0 ) ? ', ' : '';
              $img_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
            }
            $img_attrs .= $img_srcset ? ' data-sizes="auto" data-srcset="' . $img_srcset . '"' : '';
          }
          break;
        }

      }

      $html = '<img ' . $img_attrs . ' />';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Video
  public function render_lazyload_video( $params = [] ) {

    $defaults = [
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'mime_type' => '',
      'preload' => false,
      'video' => [],
      'video_url' => '',
    ];

    extract( array_merge( $defaults, $params ) );

    $video_classes = 'lazyload lazyload-item lazyload-item--video';
    $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $video_classes .= $preload ? ' lazypreload' : '';
    $video_classes = $classes ? $classes . ' ' . $video_classes : $video_classes;

    $video_url = isset($video['url']) ? $video['url'] : false;
    $mime_type = isset($video['mime_type']) ? $video['mime_type'] : false;

    if ( $video_url && $mime_type ) {
      $html = '<video
        class="' . $video_classes . '"
        src="' . $video_url . '"
        data-transition-delay="' . $delay . '"
        data-transition-duration="' . $duration . '"
        preload="none"
        muted=""
        data-autoplay=""
        data-poster=""
        loop
        playsinline
        muted
      >';
        $html .= '<source src="' . $video_url . '" type="' . $mime_type . '">';
      $html .= '</video>';
    }

    return $html;

  }

  // ---------------------------------------- Link
  public function render_link( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'active' => false,
        'block_name' => 'link',
        'classes' => '',
        'html' => '',
        'link' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $block_classes .= $style ? ' ' . $block_name . '--' . $style : '';
    $block_classes .= $active ? ' active' : '';

    // ---------------------------------------- Template
    if ( $title && $link ) {
      $html .= '<a';
      $html .= ' class="' . $block_classes . '"';
      $html .= ' href="' . $link . '"';
      $html .= ' target="' . $target . '"';
      $html .= ' title="' . str_replace( '<br>', ', ', $title ) . '"';
      $html .= $rel ? ' rel="' . $rel . '"' : '';
      $html .= '>';
        $html .= '<span class="' . $block_name . '__title">' . $title . '</span>';
        switch( $style ) {
          case 'text-with-arrow': {
            $html .= '<span class="' . $block_name . '__icon">' . $this->render_svg([ 'type' => 'icon.arrow' ]) . '</span>';
            break;
          }
        }
      $html .= '</a>';
    }

    return $html;

  }

  public function render_nu_link( $params = [] ) {

    extract(array_merge(
      [
        'classes' => 'link',
        'current_id' => false,
        'html' => '',
        'link' => '',
        'link_active' => false,
        'link_anchor' => '',
        'link_external' => '',
        'link_page' => '',
        'target' => '_blank',
        'title' => '',
        'type' => '',
      ],
      $params
    ));

    switch( $type ) {
      case 'anchor': {
        $link = $link_anchor;
        $target = '_self';
        break;
      }
      case 'external': {
        $link = $link_external;
        break;
      }
      case 'page': {
        $link = get_permalink( $link_page->ID ) ?: '';
        $link_active = $link_page->ID === $current_id ? true : false;
        $classes .= " active";
        break;
      }
    }

    return $title && $link ?
      "
        <a class='{$classes}' href='{$link}' target='{$target}' title='{$title}'>
          <span>{$title}</span>
        </a>
      " : "";

  }

  // --------------------------- Navigation Links for WP Menu
  public function render_navigation_wp_links( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'navigation',
        'current_id' => false,
        'html' => '',
        'menu_title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $menu_items = wp_get_nav_menu_items( $menu_title ) ?: [];

    // ---------------------------------------- Template
    if ( $menu_items ) {
      foreach ( $menu_items as $item ) {

        $id = $item->object_id;
        $link = $item->url;
        $title = $item->title;
        $object_type = $item->object;
        $link_type = $item->type;
        $post_parent = $item->post_parent;
        $active = ( $id == $current_id ) ? true : false;
        $target = ( 'custom' == $link_type ) ? '_blank' : '_self';
        $rel = ( 'custom' == $link_type ) ? 'noopener' : '';

        $html .= '<div
          class="' . $block_name . '__item"
          data-post-id="' . $id . '"
          ' . ( $post_parent ? 'data-post-parent-id="' . $post_parent . '"' : '' ) . '
        >';
          $html .= $this->render_link([
            'active' => $active,
            'classes' => $block_name . '__link',
            'target' => $target,
            'rel' => $rel,
            'title' => $title,
            'link' => $link,
          ]);
        $html .= '</div>';

      }
    }

    return $html;

  }

  // --------------------------- Placeholder Content
  public function render_placeholder_content( $type = 'grid', $container = 'container' ) {

    $html = '<div class="placeholder">';
      $html .= $this->render_bs_container( 'open', 'col-12', $container );

        switch ( $type ) {
          case 'content': {

            $html .= '<div class="placeholder__content rte">';

              $html .= '<h1>H1 - Placeholder Content</h1>';
              $html .= '<h2>H2 - Mauris turpis enim venenatis quis mi egestas mattis purus</h2>';
              $html .= '<p>Pellentesque at interdum enim. Suspendisse vulputate convallis mi quis auctor. Cras at urna mi. Quisque pretium tempus lacus in viverra. Sed auctor erat enim, sed accumsan orci tristique sit amet. Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</p>';
              $html .= '<p>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum. Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt a. In eget purus massa. Nulla facilisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris laoreet sapien vel odio accumsan, sed posuere libero vestibulum. Praesent est felis, tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida ac pharetra ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>';
              $html .= '<p>Vestibulum id nunc tempor, faucibus leo eu, tincidunt dui. In cursus, metus vel commodo tincidunt, odio felis facilisis arcu, in egestas erat enim id quam. In laoreet metus id luctus pellentesque. Nullam ac nunc non arcu porta maximus ac non odio. Suspendisse luctus mauris sit amet dignissim lacinia. Duis volutpat facilisis nisl quis vulputate. Sed risus purus, mollis in pulvinar in, rhoncus tristique nisi. Nunc sollicitudin sapien nibh, laoreet congue velit porttitor at. Vestibulum elementum maximus condimentum. Nam aliquam, velit ut consectetur scelerisque, sapien magna bibendum sem, ut vulputate libero massa ut justo.</p>';

              $html .= '<h3>H3 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h3>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';

              $html .= '<ol>';
                $html .= '<li>Pellentesque fringilla massa non metus cursus, vitae pretium dolor feugiat.</li>';
                $html .= '<li>Nunc bibendum sapien ac cursus sollicitudin.</li>';
                $html .= '<li>Pellentesque ut elit ac arcu luctus tincidunt.</li>';
                $html .= '<li>Morbi a arcu a lacus iaculis efficitur.</li>';
                $html .= '<li>Suspendisse efficitur nibh in lectus porttitor, vel faucibus lacus sagittis.</li>';
              $html .= '</ol>';

              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Quisque sodales tristique tincidunt. Phasellus suscipit velit vel massa feugiat placerat. Cras quis dolor iaculis nunc commodo rhoncus ac eu ex. Morbi pharetra egestas nunc, at fermentum lorem condimentum quis. Praesent sed erat id diam sollicitudin tincidunt eget vitae odio. Praesent quam odio, ultricies consectetur est eu, varius rhoncus felis. Nam vitae lectus cursus, porttitor orci in, fringilla ipsum. Vestibulum facilisis lacus turpis.</p>';

              $html .= '<h4>H4 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h4>';

              $html .= '<p>Ted aliquam augue a auctor vulputate. Nunc ut congue tellus, non mattis purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi quam ex, euismod at elit nec, placerat aliquam mi. Mauris nec blandit lorem, lacinia tristique enim. Donec suscipit lobortis arcu, non hendrerit urna rutrum in. Sed vitae metus id ex accumsan tempor. Phasellus sed commodo ex. Maecenas quis dui tortor. Vivamus quis commodo orci. Vivamus dignissim, diam malesuada imperdiet vehicula, ligula ligula ultricies libero, ut tempor turpis lacus non ipsum. Quisque non magna quis mauris ullamcorper vestibulum. Aliquam ipsum urna, faucibus congue erat vitae, maximus tincidunt nunc. Maecenas iaculis dui at velit egestas, vel commodo magna scelerisque. Nulla quis risus eu velit ornare aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
              $html .= '<p>Nam ornare laoreet vulputate. Aliquam cursus risus libero, ac tincidunt ante interdum ac. Proin scelerisque dolor non justo vestibulum, nec blandit ante fermentum. Aliquam at odio lobortis, vestibulum felis vitae, vehicula nulla. Sed dapibus sit amet nisl quis tempus. Nulla rutrum non velit id fermentum. Praesent orci nulla, finibus ac sapien nec, tristique imperdiet enim. Nulla elementum lacus vel iaculis condimentum. Vivamus eu semper ligula. Quisque ut ultricies nibh. Maecenas risus justo, gravida eu urna vestibulum, sollicitudin dictum sapien.</p>';

              $html .= '<ul>';
                $html .= '<li>Pellentesque in ligula quis nisl egestas pharetra.</li>';
                $html .= '<li>Cras elementum arcu vel leo tempus convallis.</li>';
                $html .= '<li>Pellentesque non orci id ipsum condimentum laoreet.</li>';
                $html .= '<li>Nulla maximus enim nec neque volutpat, ultrices viverra tellus facilisis.</li>';
                $html .= '<li>Morbi pharetra nunc sed tristique posuere.</li>';
                $html .= '<li>Quisque efficitur odio vitae ipsum fringilla, id aliquam ipsum pretium.</li>';
              $html .= '</ul>';

              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';
              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Mauris fermentum dui sed leo commodo efficitur. Sed quis fringilla mi. Etiam lectus odio, ultricies vestibulum leo sodales, placerat auctor arcu. Donec lorem orci, scelerisque at ante sed, vehicula condimentum purus. In ultrices facilisis nibh, sed iaculis justo vestibulum vestibulum. Duis ut felis id nunc gravida consectetur sit amet ut metus. Integer at neque imperdiet, maximus arcu nec, tincidunt turpis. Vestibulum sit amet felis quis nibh aliquam vehicula eget efficitur eros. Pellentesque semper vulputate nisl, iaculis rutrum magna molestie vitae. Nunc ac dolor ut leo suscipit gravida id non dui. Fusce vestibulum tellus elit, euismod semper libero rutrum eu. Sed tempus, lorem et dapibus interdum, arcu urna rutrum odio, sed dictum dui leo nec nunc. Vestibulum congue tortor tellus. Vivamus euismod risus a tellus laoreet, eu consequat enim ullamcorper. Etiam a iaculis odio, in faucibus purus. Pellentesque venenatis, mauris eu iaculis tempus, dui est vulputate felis, nec interdum magna velit at enim.</p>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
            $html .= '</div>';

            break;
          }
          case 'grid': {

            $html .= '<div class="placeholder__grid rte">';

              $html .= '<h1>H1 - Placeholder Grid</h1>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 12; $i++ ) {
                  $html .= '<div class="col-1">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 6; $i++ ) {
                  $html .= '<div class="col-2">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 4; $i++ ) {
                  $html .= '<div class="col-3">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 3; $i++ ) {
                  $html .= '<div class="col-4">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 2; $i++ ) {
                  $html .= '<div class="col-6">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

            $html .= '</div>';

            break;
          }
        }

      $html .= $this->render_bs_container( 'close' );
    $html .= '</div>';

    return $html;

  }

  // --------------------------- Pre-connect Scripts
  public function render_preconnect_resources( $resources = [] ) {
    $html = '';
    $relationships = [ 'preconnect', 'dns-prefetch' ];
    if ( !empty( $resources ) && !empty( $relationships ) ) {
      foreach ( $relationships as $rel ) {
        foreach ( $resources as $resource ) {
          $html .= "<link rel='{$rel}' href='{$resource}'>";
        }
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_preload_fonts( $fonts = [] ) {
    $html = '';
    if ( $fonts ) {
      foreach ( $fonts as $font ) {
        $font_src = $this->get_theme_directory('assets') . "/fonts/{$font}.woff2";
        $html .= "<link rel='preload' href='{$font_src}' as='font' type='font/woff2' crossorigin>";
      }
    }
    return $html;
  }

  // --------------------------- SEO
  public function render_seo( $enable = true ) {
    $html = '<meta name="robots" content="noindex, nofollow">';
    if ( $enable && !is_attachment( $this->get_theme_info('post_ID') ) ) {
			if ( defined( 'WPSEO_VERSION' ) ) {
				$html = '<!-- Yoast Plugin IS ACTIVE! -->';
			} else {
				$html = '<!-- Yoast Plugin IS NOT ACTIVE! -->';
				$html .= '<meta name="description" content="' . get_bloginfo( 'description' ) . '">';
			}
    }
    return $html;
  }

  // --------------------------- SVG
  public function render_svg( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => 'burger'
      ], $params
    ));

    $id = $this->get_unique_id([ 'prefix' => preg_replace('/\./', '-', $type ) ]);

    // ---------------------------------------- Template
    switch( $type ) {

      case 'abc': {
        $html = '<svg version="1.1" role="img" aria-labelledby="' . $id . '" viewBox="0 0 976 1153" fill="none">
          <title id="' . $id . '">Title of SVG</title>
          <path d="M499.947 1.17548C599.384 108.718 345.021 84.7861 228.679 175.241C172.388 219.02 216.842 276.71 280.474 298.561C343.86 320.342 431.345 312.727 508.024 304.919C600.169 295.532 696.254 287.285 787.908 297.651C879.562 308.018 965.821 341.969 974.601 386.21C979.914 412.978 956.518 439.615 921.792 460.973C887.066 482.33 841.681 499.109 796.475 515.503C691.709 553.485 584.097 591.036 502.023 639.894C419.95 688.752 365.081 751.267 384.079 812.919C393.153 842.362 418.413 870.178 430.675 899.366C456.065 959.816 421.83 1025.24 339.707 1073.2C257.585 1121.16 129.144 1150.7 -1.55225 1151.75" stroke="#7EC872" stroke-miterlimit="10" stroke-dasharray="2 2"/>
        </svg>';
        break;
      }

      case 'brand.logo': {
        $html = '';
        break;
      }

      case 'brand.monogram': {
        $html = '';
        break;
      }

      case 'brand.small': {
        $html = '';
        break;
      }

      case 'icon.email': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 25 20" style="enable-background:new 0 0 25 20;" xml:space="preserve">
          <path d="M21.8,0H3.2C1.5,0,0,1.4,0,3.2v13.6C0,18.6,1.5,20,3.2,20h18.5c1.8,0,3.2-1.4,3.2-3.2V3.2C25,1.4,23.5,0,21.8,0z M3.2,1.8
	        h18.5c0.6,0,1.1,0.4,1.3,1L12.5,10L1.9,2.8C2.1,2.2,2.6,1.8,3.2,1.8z M21.8,18.2H3.2c-0.8,0-1.4-0.6-1.4-1.4V4.9l10.1,7
	        c0.2,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l10.1-7v11.9C23.2,17.6,22.5,18.2,21.8,18.2z"/>
        </svg>';
        break;
      }

      case 'icon.email': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 25 20" style="enable-background:new 0 0 25 20;" xml:space="preserve">
          <path d="M21.8,0H3.2C1.5,0,0,1.4,0,3.2v13.6C0,18.6,1.5,20,3.2,20h18.5c1.8,0,3.2-1.4,3.2-3.2V3.2C25,1.4,23.5,0,21.8,0z M3.2,1.8
	        h18.5c0.6,0,1.1,0.4,1.3,1L12.5,10L1.9,2.8C2.1,2.2,2.6,1.8,3.2,1.8z M21.8,18.2H3.2c-0.8,0-1.4-0.6-1.4-1.4V4.9l10.1,7
	        c0.2,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l10.1-7v11.9C23.2,17.6,22.5,18.2,21.8,18.2z"/>
        </svg>';
        break;
      }

      case 'icon.facebook': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 8.6 16" style="enable-background:new 0 0 8.6 16;" xml:space="preserve">
          <path d="M8.6,0H6.3c-1,0-2,0.4-2.8,1.2C2.8,1.9,2.3,2.9,2.3,4v2.4H0v3.2h2.3V16h3.1V9.6h2.3l0.8-3.2H5.5V4c0-0.2,0.1-0.4,0.2-0.6C5.8,3.3,6,3.2,6.3,3.2h2.3V0z"/>
        </svg>';
        break;
      }

      case 'icon.instagram': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
          <g>
            <path d="M8,3.6c-0.9,0-1.7,0.3-2.4,0.7C4.8,4.8,4.3,5.5,3.9,6.3C3.6,7.1,3.5,7.9,3.7,8.8c0.2,0.8,0.6,1.6,1.2,2.2
	            c0.6,0.6,1.4,1,2.3,1.2c0.9,0.2,1.7,0.1,2.5-0.2c0.8-0.3,1.5-0.9,2-1.6c0.5-0.7,0.7-1.5,0.7-2.4c0-1.1-0.5-2.2-1.3-3
	            C10.3,4.1,9.2,3.6,8,3.6z M10,9.9c-0.5,0.5-1.3,0.8-2,0.8c-0.6,0-1.1-0.2-1.6-0.5C5.9,10,5.5,9.5,5.3,9S5.1,7.9,5.2,7.4
	            c0.1-0.5,0.4-1,0.8-1.4c0.4-0.4,0.9-0.7,1.5-0.8s1.1-0.1,1.7,0.2c0.5,0.2,1,0.6,1.3,1c0.3,0.5,0.5,1,0.5,1.6
	            C10.9,8.7,10.6,9.4,10,9.9z"/>
            <path d="M12.5,2.6c-0.6,0-1,0.5-1,1c0,0.6,0.5,1,1,1c0.6,0,1-0.5,1-1C13.5,3,13.1,2.6,12.5,2.6z"/>
            <path d="M11.3,0H4.7C2.1,0,0,2,0,4.5v6.9C0,13.9,2.1,16,4.7,16h6.7c2.6,0,4.6-2,4.6-4.5V4.5C16,2,13.9,0,11.3,0z
	             M14.7,11.5c0,0.9-0.4,1.7-1,2.3c-0.6,0.6-1.5,1-2.4,1H4.6c-0.9,0-1.8-0.4-2.4-1c-0.6-0.6-1-1.5-1-2.3V4.5c0-0.9,0.4-1.7,1-2.3
	            s1.5-1,2.4-1h6.7c0.9,0,1.8,0.4,2.4,1c0.6,0.6,1,1.5,1,2.3V11.5z"/>
          </g>
        </svg>';
        break;
      }

      case 'icon.linkedin': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
          <path d="M18,0H2C1.8,0,1.5,0.1,1.2,0.2C1,0.3,0.8,0.4,0.6,0.6C0.4,0.8,0.3,1,0.2,1.2C0.1,1.5,0,1.7,0,2v16c0,0.3,0.1,0.5,0.2,0.8
	        c0.1,0.2,0.2,0.5,0.4,0.6c0.2,0.2,0.4,0.3,0.7,0.4C1.5,19.9,1.8,20,2,20h16c0.3,0,0.5-0.1,0.8-0.2c0.2-0.1,0.5-0.2,0.7-0.4
	        c0.2-0.2,0.3-0.4,0.4-0.6c0.1-0.2,0.2-0.5,0.2-0.8V2c0-0.5-0.2-1-0.6-1.4C19,0.2,18.5,0,18,0z M6.4,15.8H3.7V7.3h2.7V15.8z M5.1,6.2
	        c-0.9,0-1.4-0.6-1.4-1.3c0-0.8,0.6-1.3,1.4-1.3c0.9,0,1.4,0.6,1.4,1.3C6.5,5.6,6,6.2,5.1,6.2z M16.2,15.8h-2.7v-4.7
	        c0-1.1-0.4-1.9-1.4-1.9c-0.7,0-1.2,0.5-1.4,1c-0.1,0.2-0.1,0.4-0.1,0.7v4.9H8V10C8,8.9,8,8,8,7.3h2.3l0.1,1.2h0.1
	        c0.3-0.6,1.2-1.4,2.7-1.4c1.8,0,3.1,1.2,3.1,3.7L16.2,15.8z"/>
        </svg>';
        break;
      }

      case 'icon.twitter': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 19.7 16" style="enable-background:new 0 0 19.7 16;" xml:space="preserve">
          <path d="M19.7,0c-0.9,0.6-1.8,1.1-2.8,1.4c-0.5-0.6-1.3-1.1-2-1.3C14-0.1,13.2,0,12.4,0.3c-0.8,0.3-1.4,0.8-1.9,1.5C10.1,2.4,9.8,3.2,9.8,4v0.9c-1.6,0-3.1-0.3-4.5-1c-1.4-0.7-2.6-1.7-3.5-3c0,0-3.6,8,4.5,11.6c-1.8,1.2-4,1.9-6.3,1.8c8,4.5,17.9,0,17.9-10.2c0-0.2,0-0.5-0.1-0.7C18.7,2.4,19.4,1.2,19.7,0z"/>
          </svg>';
        break;
      }

    }

    return $html;

  }

  public function render_svg_icon( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => ''
      ], $params
    ));

    switch( $type ) {
      case 'arrow.slim': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 51 7.4' style='enable-background:new 0 0 51 7.4;' xml:space='preserve'>
            <path class='svg-fill--white' d='M50.9,3.3l-3.2-3.2C47.5,0,47.2,0,47,0.1c-0.2,0.2-0.2,0.5,0,0.7l2.3,2.3H0v1h49.3L47,6.5
	          c-0.2,0.2-0.2,0.5,0,0.7c0.2,0.2,0.5,0.2,0.7,0L50.9,4C51,3.8,51,3.5,50.9,3.3z'/>
          </svg>
        ";
        break;
      }
      case 'camera': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 50 42' style='enable-background:new 0 0 50 42;' xml:space='preserve'>
            <g>
	            <path d='M29.4,20.5c1.2,0,2.1-0.9,2.1-2.1c0-1.2-0.9-2.1-2.1-2.1c-1.2,0-2.1,0.9-2.1,2.1
		            C27.3,19.5,28.2,20.5,29.4,20.5z'/>
	            <path d='M20.6,20.5c1.2,0,2.1-0.9,2.1-2.1c0-1.2-0.9-2.1-2.1-2.1c-1.2,0-2.1,0.9-2.1,2.1
		            C18.5,19.5,19.4,20.5,20.6,20.5z'/>
	            <path d='M25,8.4c-8,0-14.5,6.4-14.5,14.3c0,7.9,6.5,14.3,14.5,14.3c8,0,14.5-6.4,14.5-14.3C39.5,14.8,33,8.4,25,8.4z
		             M25,34.8c-6.8,0-12.3-5.5-12.3-12.1c0-6.7,5.6-12.1,12.3-12.1S37.3,16,37.3,22.7C37.3,29.3,31.8,34.8,25,34.8z'/>
	            <path d='M33.1,23.5c-0.6-0.1-1,0.1-1.3,0.9c-1.4,3.7-3.9,4.9-6.7,4.9c-2.9,0-5.4-1.3-6.7-4.9c-0.3-0.7-0.8-1-1.3-0.9
		            c-0.7,0.1-1.1,0.8-0.8,1.6c1.2,3.4,4.1,6.3,8.8,6.3c4.7,0,7.6-2.9,8.8-6.3C34.1,24.3,33.8,23.7,33.1,23.5z'/>
	            <path d='M45.7,3.3h-29c-0.2,0-0.4-0.2-0.4-0.4V2.1C16.2,1,15.2,0,14,0H9C7.8,0,6.8,1,6.8,2.1v0.7
		            c0,0.2-0.2,0.4-0.4,0.4H4.3C1.9,3.3,0,5.2,0,7.6v30.2C0,40.1,1.9,42,4.3,42h41.3c2.4,0,4.3-1.9,4.3-4.3V7.6
		            C50,5.2,48.1,3.3,45.7,3.3z M47.8,37.7c0,1.2-1,2.1-2.2,2.1H4.3c-1.2,0-2.2-0.9-2.2-2.1V7.6c0-1.2,1-2.1,2.2-2.1H9h5h31.6
		            c1.2,0,2.2,0.9,2.2,2.1V37.7z'/>
            </g>
          </svg>
        ";
        break;
      }
      case 'email': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 36 36' style='enable-background:new 0 0 36 36;' xml:space='preserve'>
            <g>
              <path d='M36,8.1C36,8,36,8,36,7.9c-0.1-2.5-2.2-4.6-4.8-4.6H4.8c-2.6,0-4.7,2-4.8,4.6C0,7.9,0,8,0,8.1c0,0,0,0,0,0v19.8
	            c0,2.7,2.2,4.8,4.8,4.8h26.4c2.7,0,4.8-2.2,4.8-4.8L36,8.1C36,8.1,36,8.1,36,8.1z M4.8,6.3h26.4c0.7,0,1.4,0.4,1.6,1.1L18,17.8
	            L3.2,7.4C3.5,6.8,4.1,6.3,4.8,6.3z M33,27.9c0,1-0.8,1.8-1.8,1.8H4.8c-1,0-1.8-0.8-1.8-1.8V11l14.1,9.9c0.3,0.2,0.6,0.3,0.9,0.3
	            s0.6-0.1,0.9-0.3L33,11V27.9z'></path>
            </g>
          </svg>
        ";
        break;
      }
      case 'instagram': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 36 36' style='enable-background:new 0 0 36 36;' xml:space='preserve'>
            <g>
	            <path d='M35.9,10.6c0-1.5-0.3-3-0.8-4.4C34.6,5,33.9,3.9,33,3c-0.9-0.9-2-1.6-3.2-2.1c-1.4-0.5-2.9-0.8-4.4-0.8
		            C23.5,0,22.9,0,18,0c-4.9,0-5.5,0-7.4,0.1c-1.5,0-3,0.3-4.4,0.8C5,1.4,3.9,2.1,3,3C2.1,3.9,1.4,5,0.9,6.2c-0.5,1.4-0.8,2.9-0.8,4.4
		            C0,12.5,0,13.1,0,18c0,4.9,0,5.5,0.1,7.4c0,1.5,0.3,3,0.8,4.4C1.4,31,2.1,32.1,3,33c0.9,0.9,2,1.6,3.2,2.1c1.4,0.5,2.9,0.8,4.4,0.8
		            C12.5,36,13.1,36,18,36c4.9,0,5.5,0,7.4-0.1c1.5,0,3-0.3,4.4-0.8c1.2-0.5,2.3-1.2,3.2-2.1c0.9-0.9,1.6-2,2.1-3.2
		            c0.5-1.4,0.8-2.9,0.8-4.4C36,23.5,36,22.9,36,18C36,13.1,36,12.5,35.9,10.6z M32.7,25.3c0,1.1-0.2,2.3-0.6,3.3
		            c-0.3,0.8-0.8,1.5-1.3,2.1c-0.6,0.6-1.3,1-2.1,1.3c-1.1,0.4-2.2,0.6-3.3,0.6c-1.9,0.1-2.5,0.1-7.3,0.1c-4.8,0-5.4,0-7.3-0.1
		            c-1.1,0-2.3-0.2-3.3-0.6c-0.8-0.3-1.5-0.8-2.1-1.3c-0.6-0.6-1.1-1.3-1.3-2.1c-0.4-1.1-0.6-2.2-0.6-3.3c-0.1-1.9-0.1-2.5-0.1-7.3
		            c0-4.8,0-5.4,0.1-7.3c0-1.1,0.2-2.3,0.6-3.3c0.3-0.8,0.8-1.5,1.3-2.1C5.9,4.7,6.6,4.3,7.4,4c1.1-0.4,2.2-0.6,3.3-0.6
		            c1.9-0.1,2.5-0.1,7.3-0.1l0,0c4.8,0,5.4,0,7.3,0.1c1.1,0,2.3,0.2,3.3,0.6c0.8,0.3,1.5,0.8,2.1,1.3c0.6,0.6,1.1,1.3,1.3,2.1
		            c0.4,1.1,0.6,2.2,0.6,3.3c0.1,1.9,0.1,2.5,0.1,7.3C32.8,22.8,32.7,23.4,32.7,25.3z'></path>
	            <path d='M24.5,11.5c-0.9-0.9-1.9-1.5-3-2C20.4,9,19.2,8.8,18,8.8c-1.8,0-3.6,0.5-5.1,1.6c-1.5,1-2.7,2.5-3.4,4.1
		            c-0.7,1.7-0.9,3.5-0.5,5.3c0.4,1.8,1.2,3.4,2.5,4.7c1.3,1.3,2.9,2.2,4.7,2.5c1.8,0.4,3.7,0.2,5.3-0.5c1.7-0.7,3.1-1.9,4.1-3.4
		            c1-1.5,1.6-3.3,1.6-5.1c0-1.2-0.2-2.4-0.7-3.5C26.1,13.3,25.4,12.3,24.5,11.5z M22.2,22.2C21.1,23.4,19.6,24,18,24
		            c-1.2,0-2.3-0.4-3.3-1s-1.8-1.6-2.2-2.7c-0.5-1.1-0.6-2.3-0.3-3.5c0.2-1.2,0.8-2.2,1.6-3.1c0.8-0.8,1.9-1.4,3.1-1.6
		            s2.4-0.1,3.5,0.3c1.1,0.5,2,1.2,2.7,2.2s1,2.1,1,3.3C24,19.6,23.4,21.1,22.2,22.2z'></path>
	            <path d='M27.6,6.2c-1.2,0-2.2,1-2.2,2.2c0,1.2,1,2.2,2.2,2.2c1.2,0,2.2-1,2.2-2.2C29.8,7.2,28.8,6.2,27.6,6.2z'></path>
            </g>
          </svg>
        ";
        break;
      }
      case 'linkedin': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 20 20' style='enable-background:new 0 0 20 20;' xml:space='preserve'>
            <g>
              <path d='M18,0H2C1.8,0,1.5,0.1,1.2,0.2C1,0.3,0.8,0.4,0.6,0.6C0.4,0.8,0.3,1,0.2,1.2C0.1,1.5,0,1.7,0,2v16c0,0.3,0.1,0.5,0.2,0.8
	            c0.1,0.2,0.2,0.5,0.4,0.6c0.2,0.2,0.4,0.3,0.7,0.4C1.5,19.9,1.8,20,2,20h16c0.3,0,0.5-0.1,0.8-0.2c0.2-0.1,0.5-0.2,0.7-0.4
	            c0.2-0.2,0.3-0.4,0.4-0.6c0.1-0.2,0.2-0.5,0.2-0.8V2c0-0.5-0.2-1-0.6-1.4C19,0.2,18.5,0,18,0z M6.4,15.8H3.7V7.3h2.7V15.8z M5.1,6.2
	            c-0.9,0-1.4-0.6-1.4-1.3c0-0.8,0.6-1.3,1.4-1.3c0.9,0,1.4,0.6,1.4,1.3C6.5,5.6,6,6.2,5.1,6.2z M16.2,15.8h-2.7v-4.7
	            c0-1.1-0.4-1.9-1.4-1.9c-0.7,0-1.2,0.5-1.4,1c-0.1,0.2-0.1,0.4-0.1,0.7v4.9H8V10C8,8.9,8,8,8,7.3h2.3l0.1,1.2h0.1
	            c0.3-0.6,1.2-1.4,2.7-1.4c1.8,0,3.1,1.2,3.1,3.7L16.2,15.8z'/></path>
            </g>
          </svg>
        ";
        break;
      }
      case 'set': {
        $html = "
          <svg viewBox='0 0 1016.2 100'>
            <path d='m156.57,93.95h-1.23v-1.13h1.23c.46,0,.66.17.66.56,0,.43-.2.56-.66.56m1.09.79v-.1c.69-.17,1.16-.6,1.16-1.43,0-1.02-.56-1.62-2.11-1.62h-2.91v5.45h1.55v-1.88h.83c.5,0,.66.17.76.56l.3,1.32h1.72l-.4-1.49c-.13-.53-.36-.79-.89-.82m-1.46,4.2c-2.51,0-4.6-2.08-4.6-4.6s2.09-4.6,4.6-4.6,4.6,2.09,4.6,4.6-2.08,4.6-4.6,4.6m0-10.25c-3.11,0-5.65,2.55-5.65,5.65s2.55,5.65,5.65,5.65,5.65-2.54,5.65-5.65-2.54-5.65-5.65-5.65m-29.26,10.91h19.61v-4.76h-13.99v-4.96h11.21v-4.76h-11.21v-3.91h13.99v-4.76h-19.61v23.15Zm-18.65,0h5.62v-18.32h8.47v-4.82h-22.55v4.82h8.47v18.32Zm-18.65,0h5.62v-23.15h-5.62v23.15Zm-23.38,0h18.68v-4.82h-13.06v-18.32h-5.62v23.15Zm-18.52-4.49c-4.7,0-7.61-2.55-7.61-7.08s2.91-7.08,7.61-7.08,7.61,2.55,7.61,7.08-2.91,7.08-7.61,7.08m0,4.89c8,0,13.36-4.76,13.36-11.97s-5.35-11.97-13.36-11.97-13.36,4.76-13.36,11.97,5.35,11.97,13.36,11.97m-25.93-13.69h-6.35v-5.16h6.35c2.38,0,3.38.99,3.38,2.58s-.99,2.58-3.38,2.58m.27-9.86h-12.23v23.15h5.62v-8.6h6.62c5.52,0,8.73-2.71,8.73-7.27s-3.2-7.27-8.73-7.27'/><path d='m85.29,61.57h19.61v-4.76h-13.99v-4.96h11.21v-4.76h-11.21v-3.91h13.99v-4.76h-19.61v23.15Zm-14.69-4.49h-6.75v-5.53h6.75c2.52,0,3.77.89,3.77,2.71s-1.26,2.81-3.77,2.81m-.26-14.16c1.98,0,2.91.66,2.91,2.08s-.93,2.12-2.91,2.12h-6.49v-4.2h6.49Zm4.53,6.12v-.13c2.41-.73,3.97-2.21,3.97-4.96,0-3.51-2.68-5.52-8.2-5.52h-12.4v23.15h12.8c5.69,0,9.02-2.28,9.02-6.68,0-3.04-1.68-5.26-5.19-5.85'/><path d='m150.07,23.91c6.18,0,10.55-2.78,10.55-7.34,0-3.94-2.51-6.02-8.5-6.98l-4.9-.79c-2.42-.4-3.24-.83-3.24-1.98s1.52-2.05,4.5-2.05c3.38,0,5.82,1.03,7.87,3.08l3.9-3.51c-2.68-2.87-6.97-4.33-11.54-4.33-5.95,0-10.12,2.58-10.12,7.04,0,3.9,2.55,5.82,7.8,6.68l5.19.86c2.35.4,3.34.96,3.34,2.15,0,1.29-1.49,2.35-4.63,2.35-3.9,0-6.98-1.72-8.96-4.07l-3.93,3.57c2.64,3.34,6.97,5.32,12.66,5.32m-29.2-.36h5.62v-8.27L136.28.4h-6.61l-5.88,9.72h-.2L117.7.4h-6.62l9.79,14.88v8.27Zm-23.85-9.45l3.6-8.9h.2l3.6,8.9h-7.41Zm11.21,9.45h5.88L104.2.4h-6.94l-9.92,23.15h5.89l2.02-4.96h10.98l2.02,4.96ZM48.84.4l7.87,23.15h6.81l5.02-17.2h.2l4.99,17.2h6.81L88.46.4h-6.22l-5.02,17.13h-.2L71.96.4h-6.58l-5.12,17.13h-.2L55.06.4h-6.22Zm-18.22,23.15h18.68v-4.82h-13.06V.4h-5.62v23.15ZM9.69,14.09l3.6-8.9h.2l3.6,8.9h-7.41Zm11.21,9.45h5.88L16.86.4h-6.94L0,23.54h5.88l2.02-4.96h10.98l2.02,4.96Z'/><path d='m835.14,62.78c1.1.02,2.38.04,3.57-.39,2.9-1.04,5.84-1.96,8.5-3.63,3.2-2.02,6.09-4.36,8.7-7.08,1.64-1.7,3.12-3.52,4.53-5.41,1.45-1.94,3.09-3.72,4.44-5.76,2.27-3.44,4.62-6.82,6.37-10.57.81-1.73,1.63-3.46,2.36-5.22.57-1.37.74-2.84.68-4.33-.02-.56-.54-.89-1.1-.73-.14.04-.29.06-.42.13-1.44.73-2.92,1.41-4.2,2.41-1.28,1.01-2.7,1.78-4.18,2.44-.69.31-1.28.69-1.78,1.26-.93,1.06-1.98,1.96-3.22,2.66-.38.21-.73.51-1,.84-1.66,2.08-3.69,3.79-5.57,5.65-1.2,1.19-2.44,2.34-3.63,3.54-.88.89-1.66,1.87-2.56,2.74-3.19,3.07-5.78,6.67-8.6,10.05-.66.78-1.15,1.7-1.73,2.55-.54.79-1.09,1.57-1.62,2.36-.25.36-.49.73-.71,1.12-.51.89-1.01,1.79-1.5,2.7-.28.51-.54,1.04-.78,1.58-.14.3.14.78.46.89.93.3,1.89.24,2.99.19m1.11,5.65c-1.53.21-3.26-.23-4.96-.67-1.8-.46-2.29-.01-2.96,1.39-.73,1.54-1.31,3.13-1.73,4.77-.44,1.71-1.01,3.38-1.64,5.04-.18.48-.39.96-.52,1.46-.55,2.08-1.59,3.96-2.49,5.89-.15.32-.39.62-.63.9-.47.56-1.07.73-1.79.6-.81-.14-1.56-.45-2.26-.85-.73-.42-1.21-.97-1.33-1.89-.12-.95-.29-1.86-.11-2.83.18-.94.2-1.9.25-2.86.13-2.51.54-4.99,1.11-7.42.37-1.57,1.01-3.07,1.55-4.59,1.06-2.99,2.39-5.86,3.84-8.68,2.25-4.39,4.28-8.89,6.44-13.32,1.13-2.31,2.31-4.6,3.7-6.77,1.95-3.03,3.91-6.06,5.89-9.07.48-.74,1.03-1.44,1.61-2.1,1.17-1.33,2.18-2.77,3-4.35.1-.2.21-.39.34-.57.44-.57,1.02-.81,1.74-.74,1.85.17,3.13,1.19,4.01,2.75.35.62.36,1.3-.11,1.87-.37.45-.79.88-1.27,1.21-.61.43-1.12.95-1.56,1.55-1.23,1.65-2.51,3.27-3.67,4.97-1.2,1.76-2.29,3.6-3.43,5.41-.07.12-.14.28-.12.41.01.12.13.23.23.32.04.04.16.04.21,0,.18-.12.37-.24.52-.4.77-.79,1.55-1.58,2.3-2.39,2.42-2.58,5.13-4.86,7.65-7.33.79-.77,1.65-1.46,2.62-2.01.95-.54,1.78-1.29,2.65-1.98,2.24-1.78,4.51-3.54,6.84-5.21,1.8-1.28,3.74-2.35,5.62-3.51,1.33-.82,2.77-1.39,4.26-1.81,1.31-.38,2.62-.25,3.9.24.84.32,1.58.8,2.15,1.5.32.4.62.83.94,1.23.79.98,1.06,2.07.83,3.31-.18,1.01-.31,2.04-.41,3.07-.07.76-.36,1.43-.68,2.09-.99,2.05-1.86,4.16-2.93,6.18-.79,1.49-1.54,3-2.36,4.48-1.5,2.71-3.15,5.31-5.05,7.77-.9,1.16-1.69,2.41-2.53,3.62-.21.3-.38.64-.61.92-2.11,2.46-4.2,4.93-6.35,7.35-1.68,1.88-3.7,3.37-5.73,4.84-.95.69-1.92,1.36-2.92,1.99-2.97,1.85-6.25,2.92-9.6,3.83-1.36.37-2.77.39-4.44.43'/><path d='m937.12,16.63c.07-.97-.65-1.55-1.56-1.22-.48.18-.95.41-1.37.69-.61.41-1.2.85-1.76,1.33-2.18,1.87-4.18,3.91-6.06,6.1-2.75,3.2-5.02,6.7-7,10.41-.09.17-.06.41-.03.62.02.2.49.2.78-.02.35-.26.69-.54,1.02-.83,1.74-1.58,3.46-3.18,5.22-4.75.76-.68,1.5-1.41,2.51-1.77.33-.12.62-.39.89-.64,2.92-2.74,3.95-3.88,6.15-6.89.66-.9,1.13-1.89,1.22-3.03m-48.6,33.36c-.08-.48-.12-.77-.17-1.06-.06-.3-.49-.64-.83-.6-.36.04-.76.06-1.06.22-.71.38-1.43.77-2.05,1.28-2.28,1.86-4.41,3.88-6.1,6.32-.33.48-.66.97-1.02,1.44-1.6,2.14-2.67,4.55-3.56,7.05-.2.55-.29,1.14-.38,1.72-.13.85.43,1.41,1.31,1.31.43-.05.89-.1,1.28-.29.85-.42,1.74-.83,2.48-1.41,1.73-1.35,3.38-2.79,4.84-4.46,2.17-2.5,3.59-5.36,4.58-8.47.33-1.04.48-2.15.68-3.05m-12.84,23.67c-1.64-.12-3.41-.22-5.01-1.1-1.62-.89-2.77-2.12-3.14-4.02-.37-1.91-.49-3.79.17-5.66.98-2.78,1.91-5.58,3.56-8.05.81-1.22,1.71-2.4,2.62-3.55,1.32-1.67,2.59-3.38,4.17-4.85,1.63-1.52,3.45-2.75,5.4-3.8.86-.47,1.75-.52,2.77-.36,1.36.22,2.5.73,3.63,1.46,3.91,2.49,8.1,2.9,12.49,1.46,1.68-.55,3.31-1.22,4.9-1.99,1.12-.54,1.89-1.34,2.42-2.46,1.23-2.59,2.49-5.17,3.8-7.73,1-1.96,2.01-3.93,3.14-5.82.91-1.51,1.99-2.92,3.02-4.35,2.37-3.32,5.24-6.16,8.27-8.86,1.27-1.13,2.64-2.13,4.1-2.98,2.31-1.34,4.73-1.44,6.95-.25,1.73.92,3.11,2.33,3.6,4.28.41,1.65.36,3.36-.63,4.9-1.76,2.74-3.8,5.24-6.27,7.37-.78.67-1.48,1.42-2.21,2.15-2.46,2.43-4.93,4.86-7.39,7.29-1.69.85-2.68,2.5-4.14,3.61-1.41,1.07-2.83,2.12-4.26,3.15-.84.6-1.68,1.21-2.58,1.7-1.16.64-1.92,1.6-2.47,2.74-1.45,2.98-2.88,5.96-4.31,8.95-.99,2.05-1.84,4.17-2.74,6.26-.62,1.43-1.06,2.9-1.47,4.39-.48,1.71-.76,3.45-.67,5.24.02.44,0,.88-.03,1.32-.12,1.39-.61,2.11-2.3,2.11-2.91.26-3.34-1-3.63-2.96-.1-.65-.12-1.33-.05-1.98.13-1.24.29-2.49.56-3.71.81-3.67,1.91-7.25,3.67-10.6.14-.26.31-.51.42-.78.56-1.44,1.16-2.86,1.95-4.2.18-.31.25-.69.36-1.04.09-.3-.15-.64-.43-.61-.36.04-.73.07-1.09.14-2.1.43-4.2.71-6.35.45-.29-.03-.59,0-.88.03-.47.06-.83.31-1.04.73-.19.4-.35.81-.5,1.22-.85,2.27-1.68,4.56-2.55,6.82-.8,2.08-1.99,3.93-3.38,5.66-1.86,2.32-4.12,4.2-6.46,5.98-1.11.85-2.4,1.42-3.69,1.98-.7.3-1.44.27-2.29.31'/><path d='m935.7,63.85c.06-.03.11-.06.17-.09-.07-.09-.14-.19-.23-.27-.01-.01-.1.06-.16.09.07.09.14.18.21.27m8.02,13.38c-.03-3.05.6-5.45,1.34-7.86,1.17-3.82,2.91-7.39,4.6-10.98.19-.4.35-.81.48-1.23.03-.1-.06-.31-.16-.36-.1-.06-.29-.03-.41.02-.2.09-.41.21-.54.37-.9,1.18-2.08,2.08-3.12,3.12-2.56,2.55-5.45,4.73-8.37,6.85-1.59,1.16-3.38,2.1-5.17,2.95-2.79,1.33-5.58,1.05-8.26-.45-1.1-.62-1.69-1.61-1.94-2.81-.16-.79-.27-1.59-.4-2.39-.03-.22-.09-.45-.04-.66.32-1.43.37-2.91.91-4.31,1.92-4.98,4.56-9.54,7.68-13.87,1.43-1.98,3.07-3.75,4.67-5.58.6-.68,1.33-.91,2.18-.73,1.95.39,3.37,1.4,3.95,3.4.17.57,0,1.01-.42,1.39-.33.29-.67.57-1.02.84-1.65,1.27-3.01,2.81-4.17,4.53-1.43,2.14-2.92,4.25-4.22,6.47-1.15,1.96-2.1,4.05-2.88,6.2-.27.76-.38,1.57-.54,2.36-.07.37.29.65.72.59.96-.14,1.85-.49,2.67-1,2.38-1.46,4.8-2.86,6.87-4.79.53-.5,1.17-.89,1.72-1.37,3.44-3,6.67-6.21,9.75-9.59,3.28-3.59,6.39-7.32,9.64-10.93,1.03-1.14,2.03-2.34,3.44-3.07.25-.13.48-.35.67-.57.99-1.18,2.01-2.35,2.93-3.59,1.4-1.89,2.64-3.91,4.11-5.74,1.67-2.07,3.11-4.29,4.71-6.4.93-1.23,1.85-2.48,2.45-3.92.2-.48.51-.89.93-1.22.55-.43,1.11-.63,1.84-.43.79.23,1.54.5,2.15,1.06.43.4.89.77,1.32,1.18.53.51.65,1.42.26,2.03-.16.25-.33.49-.54.69-1.88,1.79-3.31,3.94-4.84,6.01-.74,1-1.54,1.97-2.27,2.99-.51.71-.92,1.49-1.36,2.24-.03.06.1.31.19.33.28.06.58.1.86.06,1.52-.25,3.02-.63,4.48-1.13,2.33-.8,4.59.35,5.92,2.3.12.18.21.39.29.59.48,1.1.14,2.04-.94,2.58-.32.16-.69.28-1.04.32-.58.08-1.18.14-1.76.12-2.22-.08-4.41.2-6.58.63-1.17.23-2.33.3-3.51.22-.22-.02-.45,0-.66-.02-1.33-.17-2.2.41-2.91,1.52-2.53,3.97-5.34,7.77-7.63,11.89-.75,1.35-1.65,2.61-2.4,3.96-.57,1.02-1.02,2.11-1.52,3.17-1.01,2.12-2.02,4.25-3.02,6.38-.93,2-1.86,4-2.74,6.02-1.24,2.85-2.02,5.83-2.58,8.89-.16.88-.11,1.76-.09,2.64.02.73.06,1.47-.16,2.19-.26.82-.79,1.24-1.62,1.21-3.84-.33-3.55-1.03-3.91-3.65-.1-.72-.01-1.47-.01-1.69'/><path d='m751.37,75.95c.03-1.56.04-3.54.4-5.51.23-1.22.31-2.48.45-3.72.12-1.03.3-2.03.58-3.03.14-.49.28-1,.31-1.51.1-2.01.63-3.91,1.36-5.77.11-.27.27-.55.29-.83.18-2.23,1.03-4.29,1.66-6.4,1.16-3.88,2.46-7.7,4.34-11.31.64-1.23,1.04-2.59,1.57-3.88.31-.75.64-1.49,1.01-2.2.72-1.37,1.47-2.72,2.23-4.06.51-.89,1.05-1.78,1.63-2.62.46-.67,1.15-.87,1.95-.75.67.1,1.28.33,1.78.81.37.36.75.7,1.1,1.08.57.62.6,1.29.1,2-.13.18-.24.37-.4.53-1.85,1.78-2.87,4.08-3.91,6.35-.55,1.21-1.07,2.43-1.77,3.55-.87,1.39-1.42,2.92-1.92,4.45-.44,1.34-.95,2.62-1.61,3.86-.41.78-.75,1.56-.83,2.48-.07.87-.44,1.7-.66,2.56-.66,2.64-1.4,5.26-2.29,7.83-.1.28-.2.56-.34.81-.38.67-.52,1.38-.35,2.13.15.68.03,1.3-.19,1.95-.19.55-.31,1.14-.4,1.71-.39,2.47-.77,4.94-.93,7.44-.03.51-.2,1.01-.23,1.53-.1,1.69-.18,3.38.37,5.02.19.57.66.78,1.22.61.58-.17,1.06-.5,1.53-.87,1.5-1.2,2.87-2.53,4.09-4.01,2.3-2.79,4.37-5.73,6.48-8.66,3.05-4.24,6.11-8.47,9.2-12.68,1.7-2.31,3.58-4.49,5.15-6.88,1.41-2.15,3.1-4.1,4.48-6.28,2.56-4.05,5.52-7.82,8.41-11.63.35-.47.77-.89,1.16-1.34.38-.45.83-.85,1.14-1.35,1.1-1.78,2.61-3.18,4.06-4.65,1.6-1.62,3.18-3.28,4.79-4.9.52-.52,1.08-1,1.65-1.47.63-.52,1.38-.57,2.08-.23.8.37,1.54.85,2.3,1.31.39.24.6.64.79,1.04.2.42.11.93-.23,1.21-.17.14-.34.29-.53.4-2.99,1.83-5.28,4.45-7.67,6.95-.92.96-1.84,1.9-2.71,2.9-1.11,1.28-2.19,2.58-3.22,3.92-1.26,1.63-2.46,3.3-3.66,4.97-.77,1.07-1.44,2.22-2.36,3.19-.35.37-.59.84-.87,1.27-.28.43-.6.85-.8,1.32-.39.91-1.62,1.08-1.84,2.12-.22,1.06-.96,1.8-1.56,2.63-.56.78-1.05,1.6-1.6,2.38-.55.79-1.18,1.52-1.68,2.33-1.31,2.14-2.77,4.17-4.24,6.19-2.08,2.86-4.16,5.71-6.24,8.57-.52.71-1.07,1.41-1.55,2.15-1.68,2.6-3.48,5.1-5.41,7.52-1.38,1.73-2.74,3.46-4.26,5.06-1.02,1.07-2.18,1.98-3.35,2.87-.84.63-1.8,1-2.86,1.03-.66.02-1.33,0-1.99-.04-1.9-.12-3.11-1.22-3.88-2.85-.86-1.81-1.45-3.7-1.33-5.75.05-.81,0-1.62,0-2.86'/><path d='m978.51,51.52s.39-.08.7-.26c1.77-1.04,3.56-2.05,5.26-3.18,1.25-.84,2.01-2.11,2.44-3.56.06-.2.03-.44.03-.65,0-.36-.38-.74-.71-.68-.42.08-.89.14-1.25.36-.68.42-1.35.9-1.92,1.45-1.36,1.33-2.67,2.71-3.99,4.1-.4.42-.75.9-1.09,1.37-.12.17-.14.41-.19.62-.04.16.21.47.72.43m.08,21.76c-1.8,0-3.47-.29-4.95-1.2-3.4-2.1-5.5-5.03-5.68-9.14-.01-.29.02-.6-.04-.88-.36-1.64,0-3.22.35-4.8.09-.43.21-.86.3-1.29.4-1.87.89-3.69,2.04-5.3.67-.94,1.04-2.1,1.55-3.16.94-1.94,2.26-3.6,3.8-5.09.85-.82,1.72-1.61,2.58-2.42.91-.87,1.97-1.54,3.06-2.16.7-.4,1.48-.62,2.31-.67,1.4-.08,2.77-.14,4.13.39,1.97.77,3.3,2.14,4.1,4.06.28.68.56,1.36.79,2.06.19.56.36,1.13.45,1.7.13.82-.02,1.58-.47,2.32-.96,1.58-2.09,3.03-3.35,4.37-.87.92-2.03,1.45-3.07,2.13-.86.57-1.81,1-2.65,1.58-2.12,1.46-4.52,2.09-7.03,2.41-.22.03-.44.02-.66.04-1.41.14-1.77.43-2.16,1.77-.18.64-.34,1.28-.5,1.92-.49,1.98.18,3.61,1.49,5.05.43.47.97.66,1.58.66,1.41,0,2.77-.33,4.1-.77,2.11-.7,4.01-1.86,5.9-2.98,3.87-2.28,7.51-4.89,11.12-7.57,2.96-2.2,5.65-4.7,8.36-7.18.87-.8,1.71-1.62,2.59-2.4.66-.59,1.29-1.2,1.77-1.96.32-.51.72-.93,1.22-1.25.33-.2.7-.26,1.06-.14,1.72.58,2.87,1.73,3.43,3.46.17.53.09,1.02-.31,1.43-.36.37-.71.75-1.1,1.09-.96.81-1.82,1.7-2.67,2.63-.84.92-1.78,1.76-2.76,2.53-1.56,1.23-3.01,2.57-4.41,3.98-.82.83-1.79,1.52-2.74,2.22-1.96,1.44-4,2.74-5.87,4.29-.56.47-1.23.8-1.85,1.19-2.42,1.54-4.8,3.15-7.36,4.46-1.7.87-3.41,1.73-5.27,2.24-1.08.29-2.15.47-3.19.38'/><path d='m948.02,35.53c-1.55.05-2.53-.16-3.37-.83-.93-.74-1.48-1.68-1.46-2.9.01-.51.04-1.02.1-1.53.13-1.13.81-1.82,1.97-2.01.2-.03.4-.07.61-.08,2.22-.07,5.17,2.26,5.64,4.46.27,1.27-.27,2.21-1.57,2.55-.78.2-1.6.28-1.92.34'/><path d='m794.16,76.45c-1.39.08-2.4-.5-3.23-1.61-.72-.97-1.11-2.03-1.31-3.17-.27-1.47.84-2.75,2.31-2.72.63.02,1.26.09,1.88.19.75.12,1.41.45,1.87,1.08.5.67.99,1.36,1.43,2.07.38.61.95,1.22.62,2.02-.42,1.04-1.27,1.66-2.32,1.97-.4.12-.83.12-1.25.17'/><path d='m427.87,34.11c-.48,6.1-4.04,9.87-9.42,9.87-6.47,0-10.15-5.05-10.15-13.87s3.68-13.92,10.23-13.92c3.35,0,5.9,1.34,7.48,3.93,1.05,1.7,1.5,3.44,1.74,6.51l-5.42.4c-.16-2.35-.32-3.27-.85-4.21-.69-1.21-1.66-1.82-2.91-1.82-2.87,0-4.04,2.79-4.04,9.55,0,6.1,1.17,8.61,4.04,8.61,2.14,0,3.36-1.7,3.8-5.42l5.5.36Z'/><path d='m438.77,30.02c0,6.84.89,9.14,3.56,9.14s3.56-2.3,3.56-9.1-.93-9.06-3.52-9.06-3.6,2.27-3.6,9.02m10.11-11.24c2.18,2.39,3.23,6.03,3.23,11.33,0,9.14-3.35,13.87-9.79,13.87s-9.79-4.73-9.79-13.79,3.32-14,9.91-14c2.59,0,4.89.93,6.43,2.59'/><polygon points='466.94 16.6 470.74 33.26 475.03 16.6 483.44 16.6 483.44 43.57 477.9 43.57 477.9 21 472.4 43.57 468.07 43.57 463.06 21 463.06 43.57 458.25 43.57 458.25 16.6 466.94 16.6'/><path d='m496.44,28.97h1.82c2.75,0,3.8-1.14,3.8-4.09,0-1.37-.4-2.55-1.09-3.2-.57-.57-1.25-.73-2.71-.73h-1.82v8.01Zm1.82-12.38c3.88,0,5.42.28,6.92,1.33,2.02,1.37,3.15,3.97,3.15,7.12s-1.13,5.86-3.07,7.16c-1.58,1.05-3.23,1.41-6.71,1.41h-2.1v9.95h-5.9v-26.97h7.72Z'/><polygon points='519.23 16.6 519.23 38.51 527.93 38.51 527.93 43.57 513.33 43.57 513.33 16.6 519.23 16.6'/><path d='m541.63,22.38l-2.63,11h5.34l-2.71-11Zm3.56-5.78l7.68,26.97h-6.07l-1.33-5.54h-7.61l-1.29,5.54h-5.46l8.09-26.97h5.99Z'/><rect x='557.14' y='16.6' width='5.9' height='26.97'/><polygon points='576.5 16.6 583.38 33.34 583.38 16.6 587.99 16.6 587.99 43.57 582.69 43.57 574.52 24.04 574.52 43.57 569.91 43.57 569.91 16.6 576.5 16.6'/><polygon points='609.28 16.6 609.28 21.65 603.78 21.65 603.78 43.57 597.84 43.57 597.84 21.65 592.46 21.65 592.46 16.6 609.28 16.6'/><path d='m625.69,24.36c-.73-2.35-1.9-3.36-3.8-3.36-1.65,0-2.83,1.01-2.83,2.47,0,1.53.77,2.14,4.65,3.6,2.62,1.01,3.6,1.49,4.77,2.47,1.74,1.42,2.67,3.4,2.67,5.82,0,5.18-3.84,8.61-9.55,8.61-4.97,0-8-2.38-9.38-7.4l5.18-1.46c.76,2.83,2.06,4.04,4.32,4.04,2.06,0,3.44-1.17,3.44-2.91,0-1.54-.93-2.39-3.72-3.4-3.39-1.21-4.28-1.66-5.62-2.75-1.62-1.42-2.51-3.36-2.51-5.66,0-4.93,3.44-8.25,8.66-8.25,2.38,0,4.45.65,5.98,1.9,1.29,1.01,1.94,2.11,2.67,4.37l-4.93,1.9Z'/><polygon points='363.83 55.72 363.83 60.77 358.33 60.77 358.33 82.69 352.38 82.69 352.38 60.77 347.01 60.77 347.01 55.72 363.83 55.72'/><polygon points='384.8 55.72 384.8 60.37 374.33 60.37 374.33 66.6 382.5 66.6 382.5 71.25 374.33 71.25 374.33 77.88 384.8 77.88 384.8 82.69 368.43 82.69 368.43 55.72 384.8 55.72'/><polygon points='395.63 55.72 395.63 77.64 404.33 77.64 404.33 82.69 389.73 82.69 389.73 55.72 395.63 55.72'/><path d='m427.28,55.72c-2.67,4.97-4.12,10.92-4.12,16.86s1.46,11.93,4.12,16.87h-4.37c-3.19-4.77-4.89-10.59-4.89-16.87s1.7-12.13,4.89-16.86h4.37Z'/><path d='m439.6,74.52c0,2.71,1.09,4.25,2.99,4.25,2.02,0,2.99-1.46,2.99-4.61,0-2.75-.97-4.17-2.83-4.17-2.1,0-3.16,1.5-3.16,4.53m6.76-10.59c-.41-2.88-1.58-4.29-3.36-4.29s-2.83,1.29-3.27,3.72c-.24,1.37-.28,1.86-.41,5.38,1.34-2.22,2.71-3.08,4.97-3.08,1.38,0,2.75.45,3.84,1.22,2.06,1.5,3.19,4.04,3.19,7.32,0,5.54-3.24,8.9-8.49,8.9-2.3,0-4.16-.69-5.58-2.06-2.34-2.27-3.4-5.83-3.4-11.65,0-5.14.81-8.37,2.75-10.96,1.62-2.11,3.72-3.12,6.55-3.12,1.82,0,3.23.41,4.49,1.26,1.94,1.29,2.83,2.79,3.52,5.9l-4.81,1.46Z'/><path d='m462.03,69.07c0,8.05.48,9.7,2.75,9.7s2.75-1.7,2.75-9.58c0-4.53-.16-6.71-.57-7.85-.4-1.13-1.09-1.7-2.14-1.7-2.3,0-2.79,1.66-2.79,9.42m11.77.16c0,9.54-2.83,13.87-9.02,13.87s-8.98-4.33-8.98-13.91c0-5.5.97-9.26,2.99-11.53,1.41-1.62,3.32-2.35,6.15-2.35,6.03,0,8.86,4.41,8.86,13.91'/><path d='m488.3,60.93l-5.95,11.12h5.95v-11.12Zm5.46-5.62v16.42h3.03v4.61h-3.03v6.35h-5.9v-6.35h-9.71v-4.61l9.18-16.42h6.43Z'/><path d='m502.32,89.45c2.63-4.89,4.12-10.96,4.12-16.87s-1.5-11.93-4.12-16.86h4.37c3.19,4.81,4.89,10.59,4.89,16.86s-1.7,12.06-4.89,16.87h-4.37Z'/><path d='m536.35,60.93l-5.95,11.12h5.95v-11.12Zm5.46-5.62v16.42h3.03v4.61h-3.03v6.35h-5.9v-6.35h-9.71v-4.61l9.18-16.42h6.43Z'/><path d='m548.99,64.13c.48-2.91,1.05-4.33,2.39-5.98,1.54-1.94,3.64-2.83,6.59-2.83,5.05,0,8.33,3.07,8.33,7.85,0,3.51-1.7,6.63-5.46,9.91-1.7,1.46-3.44,2.75-5.82,4.33h11.65l-.4,5.3h-17.27v-5.3c8.82-7.52,11.08-10.35,11.08-13.91,0-2.18-.89-3.36-2.51-3.36-1.86,0-2.83,1.53-3.2,5.09l-5.38-1.09Z'/><path d='m584.2,62.51c0-2.02-1.17-3.31-2.99-3.31s-3.11,1.21-3.11,2.95c0,1.62,1.09,2.71,4.2,4.04,1.42-1.41,1.9-2.35,1.9-3.68m-7.24,12.74c0,2.31,1.49,3.76,3.88,3.76,2.1,0,3.64-1.34,3.64-3.15,0-.93-.53-1.94-1.37-2.63-.81-.61-1.41-.93-3.56-1.9-1.94,1.29-2.59,2.27-2.59,3.92m13.06-.28c0,2.19-.81,4.2-2.26,5.62-1.7,1.7-4.01,2.5-7.2,2.5-5.54,0-8.82-2.75-8.82-7.4,0-2.91,1.13-4.53,4.41-6.31-2.59-1.74-3.64-3.6-3.64-6.35,0-2.06.81-4.09,2.18-5.46,1.5-1.54,3.6-2.27,6.51-2.27,5.1,0,8.09,2.55,8.09,6.88,0,2.59-1.01,4.25-3.44,5.78,2.83,1.74,4.17,3.97,4.17,7'/><path d='m610.59,74.52c0,2.71,1.09,4.25,2.99,4.25,2.02,0,2.99-1.46,2.99-4.61,0-2.75-.97-4.17-2.83-4.17-2.11,0-3.16,1.5-3.16,4.53m6.75-10.59c-.4-2.88-1.58-4.29-3.36-4.29s-2.83,1.29-3.27,3.72c-.24,1.37-.28,1.86-.4,5.38,1.33-2.22,2.71-3.08,4.97-3.08,1.38,0,2.75.45,3.84,1.22,2.06,1.5,3.2,4.04,3.2,7.32,0,5.54-3.23,8.9-8.49,8.9-2.3,0-4.16-.69-5.58-2.06-2.35-2.27-3.4-5.83-3.4-11.65,0-5.14.81-8.37,2.75-10.96,1.61-2.11,3.72-3.12,6.55-3.12,1.82,0,3.23.41,4.49,1.26,1.94,1.29,2.83,2.79,3.52,5.9l-4.81,1.46Z'/><path d='m639.93,55.52v22.77h4.45v4.41h-14.92v-4.41h4.53v-15.94c-1.38,1.46-2.75,2.47-4.53,3.4v-5.3c2.39-1.22,4.17-2.75,5.95-4.94h4.53Z'/><path d='m666.93,55.72v4.57c-4.33,5.58-6.47,12.98-6.55,22.4h-6.43c.04-8.01,2.83-15.61,7.93-21.67h-11.4l.45-5.3h16.01Z'/><path d='m689.61,55.72v4.57c-4.33,5.58-6.47,12.98-6.55,22.4h-6.43c.04-8.01,2.83-15.61,7.93-21.67h-11.4l.45-5.3h16.01Z'/><path d='m254.84,11.29c-22.29,0-40.35,18.07-40.35,40.35s18.07,40.35,40.35,40.35,40.35-18.07,40.35-40.35-18.07-40.35-40.35-40.35Zm0,74.06c-18.59,0-33.71-15.12-33.71-33.71s15.12-33.71,33.71-33.71,33.71,15.12,33.71,33.71-15.12,33.71-33.71,33.71Z'/><path d='m254.84,24.58c-14.93,0-27.07,12.14-27.07,27.07s12.14,27.07,27.07,27.07,27.07-12.14,27.07-27.07-12.14-27.07-27.07-27.07Zm0,47.49c-11.26,0-20.42-9.16-20.42-20.42s9.16-20.43,20.42-20.43,20.43,9.16,20.43,20.43-9.16,20.42-20.43,20.42Z'/><path d='m254.84,37.86c-7.6,0-13.78,6.18-13.78,13.78s6.18,13.78,13.78,13.78,13.78-6.18,13.78-13.78-6.18-13.78-13.78-13.78Zm0,20.92c-3.94,0-7.14-3.2-7.14-7.14s3.2-7.14,7.14-7.14,7.14,3.2,7.14,7.14-3.2,7.14-7.14,7.14Z'/>
          </svg>
        ";
        break;
      }
      case 'spotify': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 36 36' style='enable-background:new 0 0 36 36;' xml:space='preserve'>
            <g>
              <path d='M18,0C8.1,0,0,8.1,0,18s8.1,18,18,18s18-8.1,18-18S27.9,0,18,0z M26.3,26.1c-0.5,0.4-0.9,0.7-1.6,0.4
	            c-4.3-2.7-9.4-3.1-15.8-1.8c-0.7,0.2-1.1-0.2-1.3-0.9c-0.2-0.7,0.2-1.1,0.9-1.4c6.8-1.6,12.8-0.9,17.5,2
	            C26.5,24.8,26.5,25.4,26.3,26.1z M28.4,21.1c-0.5,0.7-1.4,0.9-1.8,0.5c-4.9-2.9-12.1-3.8-18-2c-0.7,0-1.4-0.2-1.6-1.1
	            c-0.2-0.7,0.2-1.4,0.9-1.6c6.5-2,14.6-0.9,20.2,2.5C28.6,19.6,28.8,20.5,28.4,21.1z M28.6,16c-5.9-3.4-15.3-3.8-20.9-2
	            c-0.9,0.2-1.8-0.2-2-1.1c-0.2-0.9,0.2-1.8,1.1-2c6.3-1.8,16.9-1.6,23.6,2.5c0.9,0.5,1.1,1.6,0.7,2.2C30.6,16.2,29.5,16.4,28.6,16z'></path>
            </g>
          </svg>
        ";
        break;
      }
      case 'twitter': {
        $html = "
          <svg x='0px' y='0px' viewBox='0 0 19.7 16' style='enable-background:new 0 0 19.7 16;' xml:space='preserve'>
            <g>
              <path d='M19.7,0c-0.9,0.6-1.8,1.1-2.8,1.4c-0.5-0.6-1.3-1.1-2-1.3C14-0.1,13.2,0,12.4,0.3c-0.8,0.3-1.4,0.8-1.9,1.5C10.1,2.4,9.8,3.2,9.8,4v0.9c-1.6,0-3.1-0.3-4.5-1c-1.4-0.7-2.6-1.7-3.5-3c0,0-3.6,8,4.5,11.6c-1.8,1.2-4,1.9-6.3,1.8c8,4.5,17.9,0,17.9-10.2c0-0.2,0-0.5-0.1-0.7C18.7,2.4,19.4,1.2,19.7,0z'/></path>
            </g>
          </svg>
        ";
        break;
      }
    }

    return $html;

  }

  public function render_svg_logo( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => ''
      ], $params
    ));

    switch( $type ) {
      case 'main': {
        $html = "
          <svg viewBox='0 0 463.08 100'><path d='m111.66,89.85c0,2.78-2.26,5.04-5.04,5.04H10.14c-2.78,0-5.03-2.26-5.03-5.04V18.02c0-2.78,2.26-5.04,5.03-5.04h96.48c2.78,0,5.04,2.26,5.04,5.04v71.83Zm-5.04-81.98H38.87c-.58,0-1.05-.47-1.05-1.05v-1.73c0-2.81-2.28-5.1-5.1-5.1h-11.64c-2.81,0-5.1,2.28-5.1,5.1v1.78c0,.55-.45,1-1,1h-4.84C4.54,7.87,0,12.41,0,18.02v71.84c0,5.6,4.54,10.15,10.15,10.15h96.48c5.6,0,10.15-4.54,10.15-10.15V18.02c0-5.6-4.54-10.15-10.15-10.15'/><path d='m58.39,19.96c-18.76,0-33.97,15.21-33.97,33.97s15.21,33.97,33.97,33.97,33.97-15.21,33.97-33.97-15.21-33.97-33.97-33.97Zm0,62.35c-15.65,0-28.38-12.73-28.38-28.38s12.73-28.38,28.38-28.38,28.38,12.73,28.38,28.38-12.73,28.38-28.38,28.38Z'/><path d='m58.39,31.15c-12.57,0-22.79,10.22-22.79,22.79s10.22,22.79,22.79,22.79,22.79-10.22,22.79-22.79-10.22-22.79-22.79-22.79Zm0,39.99c-9.48,0-17.2-7.71-17.2-17.2s7.71-17.2,17.2-17.2,17.2,7.71,17.2,17.2-7.71,17.2-17.2,17.2Z'/><path d='m58.39,42.33c-6.4,0-11.6,5.21-11.6,11.6s5.21,11.6,11.6,11.6,11.6-5.21,11.6-11.6-5.21-11.6-11.6-11.6Zm0,17.62c-3.31,0-6.01-2.7-6.01-6.01s2.7-6.01,6.01-6.01,6.01,2.7,6.01,6.01-2.7,6.01-6.01,6.01Z'/><path d='m149.19,3.99h7.16l7.54,20.28h.23l7.54-20.28h7.17l-10.88,26.54h-7.89l-10.88-26.54Z'/><path d='m181.25,3.99h22.48v5.46h-16.04v4.47h12.85v5.46h-12.85v5.69h16.04v5.46h-22.48V3.99Z'/><path d='m233.03,30.53h-6.9l-1.71-6.63c-.57-2.27-1.9-2.99-4.06-2.99h-6.33v9.63h-6.45V3.99h14.79c7.2,0,10.08,2.88,10.08,7.81,0,3.94-2.35,5.95-5.72,6.67v.23c2.58.19,3.64,1.37,4.36,4.17l1.93,7.66Zm-7.01-18.08c0-1.93-1.21-3.07-4.02-3.07h-7.96v6.14h7.96c2.81,0,4.02-1.14,4.02-3.07Z'/><path d='m244.47,21.05l-11.22-17.06h7.58l6.75,11.15h.23l6.75-11.15h7.58l-11.22,17.06v9.48h-6.45v-9.48Z'/><path d='m296.02,12.33c0,5.23-3.68,8.34-10.01,8.34h-7.58v9.86h-6.44V3.99h14.03c6.33,0,10.01,3.11,10.01,8.34Zm-6.45,0c0-1.82-1.14-2.96-3.87-2.96h-7.28v5.91h7.28c2.73,0,3.87-1.14,3.87-2.96Z'/><path d='m297.98,17.26c0-8.26,6.14-13.72,15.32-13.72s15.32,5.46,15.32,13.72-6.14,13.72-15.32,13.72-15.32-5.46-15.32-13.72Zm24.04,0c0-5.19-3.34-8.11-8.72-8.11s-8.72,2.92-8.72,8.11,3.34,8.11,8.72,8.11,8.72-2.92,8.72-8.11Z'/><path d='m332.39,3.99h6.45v21h14.97v5.53h-21.42V3.99Z'/><path d='m363.51,3.99v26.54h-6.45V3.99h6.45Z'/><path d='m376.32,9.52h-9.7V3.99h25.86v5.54h-9.7v21h-6.45V9.52Z'/><path d='m395.58,3.99h22.48v5.46h-16.04v4.47h12.85v5.46h-12.85v5.69h16.04v5.46h-22.48V3.99Z'/><path d='m176.03,47.07c0,5.23-3.68,8.34-10.01,8.34h-7.58v9.86h-6.45v-26.54h14.03c6.33,0,10.01,3.11,10.01,8.34Zm-6.44,0c0-1.82-1.14-2.96-3.87-2.96h-7.28v5.91h7.28c2.73,0,3.87-1.14,3.87-2.96Z'/><path d='m204.87,65.26h-6.9l-1.71-6.63c-.57-2.27-1.9-2.99-4.06-2.99h-6.33v9.63h-6.44v-26.54h14.78c7.2,0,10.08,2.88,10.08,7.81,0,3.94-2.35,5.95-5.72,6.67v.23c2.58.19,3.64,1.37,4.36,4.17l1.93,7.66Zm-7.01-18.08c0-1.93-1.21-3.07-4.02-3.07h-7.96v6.14h7.96c2.81,0,4.02-1.14,4.02-3.07Z'/><path d='m207.14,51.99c0-8.26,6.14-13.72,15.32-13.72s15.32,5.46,15.32,13.72-6.14,13.72-15.32,13.72-15.32-5.46-15.32-13.72Zm24.04,0c0-5.19-3.34-8.11-8.72-8.11s-8.72,2.92-8.72,8.11,3.34,8.11,8.72,8.11,8.72-2.92,8.72-8.11Z'/><path d='m268.66,51.99c0,7.81-5.31,13.27-14.71,13.27h-12.39v-26.54h12.39c9.4,0,14.71,5.46,14.71,13.27Zm-6.6,0c0-4.85-2.96-7.81-8.72-7.81h-5.34v15.62h5.34c5.76,0,8.72-2.96,8.72-7.81Z'/><path d='m272.29,53.36v-14.63h6.45v13.99c0,5.16,2.24,7.39,6.75,7.39s6.75-2.24,6.75-7.39v-13.99h6.45v14.63c0,8.07-4.21,12.36-13.19,12.36s-13.19-4.28-13.19-12.36Z'/><path d='m317.5,65.72c-9.06,0-15.2-5.46-15.2-13.72s6.14-13.72,15.2-13.72c7.43,0,12.24,3.45,14.18,8.68l-5.95,2.2c-1.21-3.3-4.02-5.27-8.23-5.27-5.19,0-8.61,2.92-8.61,8.11s3.41,8.11,8.61,8.11c4.28,0,6.94-2.05,8.3-5.46l5.95,2.16c-1.9,5.46-6.82,8.91-14.25,8.91Z'/><path d='m342.59,44.26h-9.7v-5.54h25.86v5.54h-9.7v21h-6.45v-21Z'/><path d='m368.29,38.73v26.54h-6.45v-26.54h6.45Z'/><path d='m372.08,51.99c0-8.26,6.14-13.72,15.32-13.72s15.32,5.46,15.32,13.72-6.14,13.72-15.32,13.72-15.32-5.46-15.32-13.72Zm24.04,0c0-5.19-3.34-8.11-8.72-8.11s-8.72,2.92-8.72,8.11,3.34,8.11,8.72,8.11,8.72-2.92,8.72-8.11Z'/><path d='m406.49,38.73h8.72l11.56,19.3h.23v-19.3h6.29v26.54h-8.72l-11.56-19.3h-.23v19.3h-6.29v-26.54Z'/><path d='m436.47,59.58l4.51-4.09c2.28,2.69,5.8,4.66,10.27,4.66,3.6,0,5.31-1.21,5.31-2.69,0-1.36-1.14-2.01-3.83-2.46l-5.95-.99c-6.03-.99-8.95-3.18-8.95-7.66,0-5.12,4.78-8.08,11.6-8.08,5.23,0,10.16,1.67,13.23,4.97l-4.47,4.02c-2.35-2.35-5.15-3.53-9.02-3.53-3.41,0-5.16.99-5.16,2.35s.95,1.82,3.71,2.27l5.61.91c6.86,1.1,9.74,3.49,9.74,8,0,5.23-5,8.42-12.09,8.42-6.52,0-11.49-2.27-14.52-6.1Z'/><path d='m151.99,73.46h6.45v21h14.97v5.53h-21.42v-26.54Z'/><path d='m179.02,79h-9.7v-5.54h25.86v5.54h-9.7v21h-6.45v-21Z'/><path d='m225.37,86.73c0,7.81-5.31,13.27-14.71,13.27h-12.39v-26.54h12.39c9.4,0,14.71,5.46,14.71,13.27Zm-6.6,0c0-4.85-2.96-7.81-8.72-7.81h-5.34v15.62h5.34c5.76,0,8.72-2.96,8.72-7.81Z'/><path d='m233.63,100h-7.05v-5.69h7.05v5.69Z'/>
          </svg>
        ";
        break;
      }
    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
